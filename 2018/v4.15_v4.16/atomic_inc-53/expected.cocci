// lots of fps because transformation was done by type not by file
@r@
type T;
T *e;
identifier fld;
@@
-                       atomic_set(&e->fld, 1)
+                       refcount_set(&e->fld, 1)

@@
type r.T;
T *e;
identifier r.fld;
@@
-       WARN_ON(atomic_read(&e->fld) <= 0);

@@
type r.T;
T *e;
identifier r.fld;
@@
- atomic_dec_and_test
+ refcount_dec_and_test
        (&e->fld)

@@
type r.T;
T *e;
identifier r.fld;
expression e1;
@@
- atomic_dec_and_lock
+ refcount_dec_and_lock
        (&e->fld,e1)

@@
type r.T;
T *e;
identifier r.fld;
@@
- atomic_inc_not_zero
+ refcount_inc_not_zero
        (&e->fld)

@@
type r.T;
T *e;
identifier r.fld;
@@
- atomic_read
+ refcount_read
        (&e->fld)

@@
type r.T;
T *e;
identifier r.fld;
@@
- atomic_inc
+ refcount_inc
        (&e->fld)

@@
type r.T;
T *e;
identifier r.fld;
@@
- atomic_dec
+ refcount_dec
        (&e->fld)

@@
type r.T;
identifier r.fld;
@@
T {
  ...
- atomic_t
+ refcount_t
    fld;
  ... };

// ------------------

@@
type T;
struct nlm_rqst *e;
@@
-                       atomic_set(&e->a_count, 1)
+                       refcount_set(&e->a_count, 1)

@@
struct nlm_rqst *e;
@@
-       WARN_ON(atomic_read(&e->a_count) <= 0);

@@
struct nlm_rqst *e;
@@
- atomic_dec_and_test
+ refcount_dec_and_test
        (&e->a_count)

@@
struct nlm_rqst *e;
expression e1;
@@
- atomic_dec_and_lock
+ refcount_dec_and_lock
        (&e->a_count,e1)

@@
struct nlm_rqst *e;
@@
- atomic_inc_not_zero
+ refcount_inc_not_zero
        (&e->a_count)

@@
struct nlm_rqst *e;
@@
- atomic_read
+ refcount_read
        (&e->a_count)

@@
struct nlm_rqst *e;
@@
- atomic_inc
+ refcount_inc
        (&e->a_count)

@@
struct nlm_rqst *e;
@@
- atomic_dec
+ refcount_dec
        (&e->a_count)

// ------------------

@@
type T;
struct posix_acl *e;
@@
-                       atomic_set(&e->a_refcount, 1)
+                       refcount_set(&e->a_refcount, 1)

@@
struct posix_acl *e;
@@
-       WARN_ON(atomic_read(&e->a_refcount) <= 0);

@@
struct posix_acl *e;
@@
- atomic_dec_and_test
+ refcount_dec_and_test
        (&e->a_refcount)

@@
struct posix_acl *e;
expression e1;
@@
- atomic_dec_and_lock
+ refcount_dec_and_lock
        (&e->a_refcount,e1)

@@
struct posix_acl *e;
@@
- atomic_inc_not_zero
+ refcount_inc_not_zero
        (&e->a_refcount)

@@
struct posix_acl *e;
@@
- atomic_read
+ refcount_read
        (&e->a_refcount)

@@
struct posix_acl *e;
@@
- atomic_inc
+ refcount_inc
        (&e->a_refcount)

@@
struct posix_acl *e;
@@
- atomic_dec
+ refcount_dec
        (&e->a_refcount)

// ------------------

@@
type T;
struct nsm_handle *e;
@@
-                       atomic_set(&e->sm_count, 1)
+                       refcount_set(&e->sm_count, 1)

@@
struct nsm_handle *e;
@@
-       WARN_ON(atomic_read(&e->sm_count) <= 0);

@@
struct nsm_handle *e;
@@
- atomic_dec_and_test
+ refcount_dec_and_test
        (&e->sm_count)

@@
struct nsm_handle *e;
expression e1;
@@
- atomic_dec_and_lock
+ refcount_dec_and_lock
        (&e->sm_count,e1)

@@
struct nsm_handle *e;
@@
- atomic_inc_not_zero
+ refcount_inc_not_zero
        (&e->sm_count)

@@
struct nsm_handle *e;
@@
- atomic_read
+ refcount_read
        (&e->sm_count)

@@
struct nsm_handle *e;
@@
- atomic_inc
+ refcount_inc
        (&e->sm_count)

@@
struct nsm_handle *e;
@@
- atomic_dec
+ refcount_dec
        (&e->sm_count)
