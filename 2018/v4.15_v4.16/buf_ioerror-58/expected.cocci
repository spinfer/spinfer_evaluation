@@
expression bp,e;
@@

-               xfs_buf_ioerror(bp, e);
-               xfs_verifier_error(bp);
+               xfs_verifier_error(bp, e);

@@
expression bp;
@@

	if (...)
-               xfs_buf_ioerror(bp, -EFSBADCRC);
+               xfs_verifier_error(bp, -EFSBADCRC);
        else if (...)
-               xfs_buf_ioerror(bp, -EFSCORRUPTED);
-
-       if (bp->b_error)
-               xfs_verifier_error(bp);
+               xfs_verifier_error(bp, -EFSCORRUPTED);

@@
expression bp;
statement S;
@@

	if (...)
-               xfs_buf_ioerror(bp, -EFSBADCRC);
+               xfs_verifier_error(bp, -EFSBADCRC);
        else if (...)
-               xfs_buf_ioerror(bp, -EFSCORRUPTED);
+               xfs_verifier_error(bp, -EFSCORRUPTED);
-
       if (bp->b_error)
- {
		S
-               xfs_verifier_error(bp);
- }

@@
expression bp;
statement S;
@@

	if (...)
-               xfs_buf_ioerror(bp, -EFSBADCRC);
+               xfs_verifier_error(bp, -EFSBADCRC);
        else if (...)
-               xfs_buf_ioerror(bp, -EFSCORRUPTED);
+               xfs_verifier_error(bp, -EFSCORRUPTED);
-
       if (bp->b_error)
- {
-               xfs_verifier_error(bp);
		S
- }
