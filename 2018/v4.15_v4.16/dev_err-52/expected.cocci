@@
struct pci_dev *dv;
@@

-dev_err(&dv->dev,
+pci_err(dv,
	...)

@@
struct pci_dev *dv;
@@

-dev_alert(&dv->dev,
+pci_alert(dv,
	...)

@@
struct pci_dev *dv;
@@

-dev_warn(&dv->dev,
+pci_warn(dv,
	...)

@@
struct pci_dev *dv;
@@

-dev_info(&dv->dev,
+pci_info(dv,
	...)

@@
struct pci_dev *dv;
@@

-dev_dbg(&dv->dev,
+pci_dbg(dv,
	...)

@@
struct pci_dev *dv;
@@

-dev_notice(&dv->dev,
+pci_notice(dv,
	...)

@@
struct pci_dev *dv;
expression i;
@@

-dev_printk(i,&dv->dev,
+pci_printk(i,dv,
	...)

// ----------------------------------

@@
struct pnv_php_slot *dv;
@@

-dev_err(&dv->pdev->dev,
+pci_err(dv->pdev,
	...)

@@
struct pnv_php_slot *dv;
@@

-dev_alert(&dv->pdev->dev,
+pci_alert(dv->pdev,
	...)

@@
struct pnv_php_slot *dv;
@@

-dev_warn(&dv->pdev->dev,
+pci_warn(dv->pdev,
	...)

@@
struct pnv_php_slot *dv;
@@

-dev_info(&dv->pdev->dev,
+pci_info(dv->pdev,
	...)

@@
struct pnv_php_slot *dv;
@@

-dev_dbg(&dv->pdev->dev,
+pci_dbg(dv->pdev,
	...)

@@
struct pnv_php_slot *dv;
@@

-dev_notice(&dv->pdev->dev,
+pci_notice(dv->pdev,
	...)

@@
struct pnv_php_slot *dv;
expression i;
@@

-dev_printk(i,&dv->pdev->dev,
+pci_printk(i,dv->pdev,
	...)

// ----------------------------------

@@
struct pci_bus *dv;
@@

-dev_err(&dv->self->dev,
+pci_err(dv->self,
	...)

@@
struct pci_bus *dv;
@@

-dev_alert(&dv->self->dev,
+pci_alert(dv->self,
	...)

@@
struct pci_bus *dv;
@@

-dev_warn(&dv->self->dev,
+pci_warn(dv->self,
	...)

@@
struct pci_bus *dv;
@@

-dev_info(&dv->self->dev,
+pci_info(dv->self,
	...)

@@
struct pci_bus *dv;
@@

-dev_dbg(&dv->self->dev,
+pci_dbg(dv->self,
	...)

@@
struct pci_bus *dv;
@@

-dev_notice(&dv->self->dev,
+pci_notice(dv->self,
	...)

@@
struct pci_bus *dv;
expression i;
@@

-dev_printk(i,&dv->self->dev,
+pci_printk(i,dv->self,
	...)

// ----------------------------------

@@
struct slot *dv;
@@

-dev_err(&dv->pci_bus->self->dev,
+pci_err(dv->pci_bus->self,
	...)

@@
struct slot *dv;
@@

-dev_alert(&dv->pci_bus->self->dev,
+pci_alert(dv->pci_bus->self,
	...)

@@
struct slot *dv;
@@

-dev_warn(&dv->pci_bus->self->dev,
+pci_warn(dv->pci_bus->self,
	...)

@@
struct slot *dv;
@@

-dev_info(&dv->pci_bus->self->dev,
+pci_info(dv->pci_bus->self,
	...)

@@
struct slot *dv;
@@

-dev_dbg(&dv->pci_bus->self->dev,
+pci_dbg(dv->pci_bus->self,
	...)

@@
struct slot *dv;
@@

-dev_notice(&dv->pci_bus->self->dev,
+pci_notice(dv->pci_bus->self,
	...)

@@
struct slot *dv;
expression i;
@@

-dev_printk(i,&dv->pci_bus->self->dev,
+pci_printk(i,dv->pci_bus->self,
	...)
