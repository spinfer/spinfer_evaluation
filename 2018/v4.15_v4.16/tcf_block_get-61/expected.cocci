@@
identifier f,extack;
expression e1,e2,e3;
@@

f(...,struct netlink_ext_ack *extack,...) { <...
tcf_block_get(e1,e2,e3
+              ,extack
             )
...> }

@@
identifier f,extack;
expression e1,e2,e3;
@@

f(...,struct netlink_ext_ack *extack,...) { <...
tcf_block_get_ext(e1,e2,e3
+                   ,extack
             )
...> }
