// isomorphic of variable declaration positions, what appear to be some bug
// fixes, and one patch combining in another issue...
@@
identifier f;
expression e1,e2,x,y;
symbol ctx;
@@

f(...) {
++ struct ttm_operation_ctx ctx = { e1,e2 };
<+...
               ttm_bo_validate(x,y,
-                               e1,e2
+              			&ctx
			      )
...+> }