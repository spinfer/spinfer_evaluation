@@
expression e1,e2,e3,e4,e5;
identifier print;
constant char[] c;
type T;
@@

        e1 = (T)
-               dma_pool_alloc
+               dma_pool_zalloc
			(e2,e3,e4);
        <... when != e1
(
 	e1 == NULL
|
	print(...,c,...,e1,...)
)
	...>
-      memset(e1, 0, e5);

@@
expression e1,e2,e3,e4,e5;
identifier e;
identifier print;
constant char[] c;
type T,T1,T2,T3;
@@

-	T3 e;
	... when != e
        (<+...e1 = (<+...(T)
-               dma_pool_alloc
+               dma_pool_zalloc
			(e2,e3,e4)...+>)...+>);
        <... when != e1
	     when != e
(
 	e1 == NULL
|
	print(...,c,...,e1,...)
)
	...>
-	e = (T2)e1;
	... when != e
-      memset(e, 0, e5);
       ... when != e
