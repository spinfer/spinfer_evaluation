@@
type T,T1;
idexpression T *e;
expression f;
@@

f(...,(T1)e,...,sizeof(
- T
+ *e
 ),...)

@@
type T,T1;
idexpression T e;
expression f;
@@

f(...,(T1)&e,...,sizeof(
- T
+ e
 ),...)

@@
type T;
idexpression T *e;
identifier f;
@@

e = f(...,sizeof(
- T
+ *e
 ),...)

// ------------------------------------------------------

@@
expression e1,e2;
type T;
@@

memset (&e1,e2,
- sizeof(T)
+ sizeof(e1)
  )

@@
expression e1,e2;
type T;
@@

memcpy (&e1,e2,
- sizeof(T)
+ sizeof(e1)
  )

@@
expression e1,e2;
type T;
@@

e1 = kmalloc (
- sizeof(T)
+ sizeof(*e1)
  ,e2)

@@
expression e1,e2,x,y;
type T;
@@

e1 = devm_kcalloc (x,y,
- sizeof(T)
+ sizeof(*e1)
  ,e2)

@@
expression e1,e2,y;
type T;
@@

e1 = kcalloc (y,
- sizeof(T)
+ sizeof(*e1)
  ,e2)

@@
expression e1,e2;
type T;
@@

e1 = kzalloc (
- sizeof(T)
+ sizeof(*e1)
  ,e2)

@@
expression e1,e2,e3;
type T;
@@

	dma_map_single(e1,&e2,
-		sizeof(T)
+		sizeof(e2)
		,e3)

@@
expression e,e1,e2;
type T;
@@

e = s3c_set_platdata(e1,
-               sizeof(T)
+               sizeof(*e)
		,e2)
