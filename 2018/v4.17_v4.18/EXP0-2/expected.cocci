@@
symbol current;
@@

-       current->audit_context
+       audit_context()

@@
symbol current;
identifier tsk;
type T;
expression e;
@@

(
-	T tsk = current;
	<...
(
        tsk->audit_context = e
|
-       tsk->audit_context
+       audit_context()
|
-	tsk
+	current
)
	...>
&
	T tsk = current;
	... when exists
	tsk->audit_context
	... when any
)
