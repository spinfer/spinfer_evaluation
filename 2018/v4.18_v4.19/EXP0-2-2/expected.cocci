@@
expression p;
@@

- p->tcfa_bindcnt--
+ atomic_dec(&p->tcfa_bindcnt)

@@
expression p;
@@

- p->tcfa_bindcnt++
+ atomic_inc(&p->tcfa_bindcnt)

@@
expression p;
expression e;
@@

- p->tcfa_bindcnt = e
+ atomic_set(&p->tcfa_bindcnt,e)

@@
expression p;
@@

(
  &p->tcfa_bindcnt
|
- p->tcfa_bindcnt
+ atomic_read(&p->tcfa_bindcnt)
)




@@
expression p;
expression e;
@@

- p->tcfa_refcnt = e
+ refcount_set(&p->tcfa_refcnt,e)

@@
expression p;
statement S1,S2;
@@

-               p->tcfa_refcnt--;
                ... when != p->tcfa_refcnt
                if (<+...
-                        p->tcfa_refcnt <= 0
+                        refcount_dec_and_test(&p->tcfa_refcnt)
                    ...+>) S1 else S2

@@
expression p;
@@

- p->tcfa_refcnt--
+ refcount_dec(&p->tcfa_refcnt)

@@
expression p;
@@

- p->tcfa_refcnt++
+ refcount_inc(&p->tcfa_refcnt)

@@
expression p;
@@

(
  &p->tcfa_refcnt
|
- p->tcfa_refcnt
+ refcount_read(&p->tcfa_refcnt)
)



@@
expression p;
@@

- p->tcf_bindcnt--
+ atomic_dec(&p->tcf_bindcnt)

@@
expression p;
@@

- p->tcf_bindcnt++
+ atomic_inc(&p->tcf_bindcnt)

@@
expression p;
expression e;
@@

- p->tcf_bindcnt = e
+ atomic_set(&p->tcf_bindcnt,e)

@@
expression p;
@@

- p->tcf_bindcnt
+ atomic_read(&p->tcf_bindcnt)




@@
expression p;
expression e;
@@

- p->tcf_refcnt = e
+ refcount_set(&p->tcf_refcnt,e)

@@
expression p;
statement S1,S2;
@@

-               p->tcf_refcnt--;
                ... when != p->tcf_refcnt
                if (<+...
-                        p->tcf_refcnt <= 0
+                        refcount_dec_and_test(&p->tcf_refcnt)
                    ...+>) S1 else S2

@@
expression p;
@@

- p->tcf_refcnt--
+ refcount_dec(&p->tcf_refcnt)

@@
expression p;
@@

- p->tcf_refcnt++
+ refcount_inc(&p->tcf_refcnt)

@@
expression p;
@@

- p->tcf_refcnt
+ refcount_read(&p->tcf_refcnt)
