@@
expression dp;
@@
-               IRELE(dp);
+               xfs_irele(dp);

@@
expression dp;
@@
-               iput(VFS_I(dp));
+               xfs_irele(dp);

@@
expression e,e1,inode;
@@
   inode = VFS_I(e);
   ... when != inode = e1;
?- iput(inode);
+  xfs_irele(e);
