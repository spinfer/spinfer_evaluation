@@
expression e1,e2,e3;
@@
amdgpu_irq_add_id(e1,e2,<+...
- 7
+ VISLANDS30_IV_SRCID_D1_V_UPDATE_INT
...+>,e3)

@@
expression e1,e2,e3;
@@
amdgpu_irq_add_id(e1,e2,<+...
- 8
+ VISLANDS30_IV_SRCID_D1_GRPH_PFLIP
...+>,e3)

@@
expression e1,e2,e3;
@@
amdgpu_irq_add_id(e1,e2,<+...
- 9
+ VISLANDS30_IV_SRCID_D2_V_UPDATE_INT
...+>,e3)

@@
expression e1,e2,e3;
@@
amdgpu_irq_add_id(e1,e2,<+...
- 10
+ VISLANDS30_IV_SRCID_D2_GRPH_PFLIP
...+>,e3)

@@
expression e1,e2,e3;
@@
amdgpu_irq_add_id(e1,e2,<+...
- 11
+ VISLANDS30_IV_SRCID_D3_V_UPDATE_INT
...+>,e3)

@@
expression e1,e2,e3;
@@
amdgpu_irq_add_id(e1,e2,<+...
- 12
+ VISLANDS30_IV_SRCID_D3_GRPH_PFLIP
...+>,e3)

@@
expression e1,e2,e3;
@@
amdgpu_irq_add_id(e1,e2,<+...
- 13
+ VISLANDS30_IV_SRCID_D4_V_UPDATE_INT
...+>,e3)

@@
expression e1,e2,e3;
@@
amdgpu_irq_add_id(e1,e2,<+...
- 14
+ VISLANDS30_IV_SRCID_D4_GRPH_PFLIP
...+>,e3)

@@
expression e1,e2,e3;
@@
amdgpu_irq_add_id(e1,e2,<+...
- 15
+ VISLANDS30_IV_SRCID_D5_V_UPDATE_INT
...+>,e3)

@@
expression e1,e2,e3;
@@
amdgpu_irq_add_id(e1,e2,<+...
- 16
+ VISLANDS30_IV_SRCID_D5_GRPH_PFLIP
...+>,e3)
@@
expression e1,e2,e3;
@@
amdgpu_irq_add_id(e1,e2,<+...
- 17
+ VISLANDS30_IV_SRCID_D6_V_UPDATE_INT
...+>,e3)

@@
expression e1,e2,e3;
@@
amdgpu_irq_add_id(e1,e2,<+...
- 18
+ VISLANDS30_IV_SRCID_D6_GRPH_PFLIP
...+>,e3)

@@
expression e1,e2,e3;
@@
amdgpu_irq_add_id(e1,e2,<+...
- 42
+ VISLANDS30_IV_SRCID_HOTPLUG_DETECT_A
...+>,e3)

@@
expression e1,e2,e3;
@@
amdgpu_irq_add_id(e1,e2,<+...
- 83
+ VISLANDS30_IV_SRCID_GPIO_19
...+>,e3)

@@
expression e1,e2,e3;
@@
amdgpu_irq_add_id(e1,e2,<+...
- 96
+ VISLANDS30_IV_SRCID_SRBM_READ_TIMEOUT_ERR
...+>,e3)
@@
expression e1,e2,e3;
@@
amdgpu_irq_add_id(e1,e2,<+...
- 97
+ VISLANDS30_IV_SRCID_SRBM_CTX_SWITCH
...+>,e3)

@@
expression e1,e2,e3;
@@
amdgpu_irq_add_id(e1,e2,<+...
- 98
+ VISLANDS30_IV_SRBM_REG_ACCESS_ERROR
...+>,e3)

@@
expression e1,e2,e3;
@@
amdgpu_irq_add_id(e1,e2,<+...
- 119
+ VISLANDS30_IV_SRCID_UVD_ENC_GEN_PURP 
...+>,e3)
@@
expression e1,e2,e3;
@@
amdgpu_irq_add_id(e1,e2,<+...
- 124
+ VISLANDS30_IV_SRCID_UVD_SYSTEM_MESSAGE 
...+>,e3)

@@
expression e1,e2,e3;
@@
amdgpu_irq_add_id(e1,e2,<+...
- 135
+ VISLANDS30_IV_SRCID_BIF_PF_VF_MSGBUF_VALID 
...+>,e3)
@@
expression e1,e2,e3;
@@
amdgpu_irq_add_id(e1,e2,<+...
- 138
+ VISLANDS30_IV_SRCID_BIF_VF_PF_MSGBUF_ACK
...+>,e3)

@@
expression e1,e2,e3;
@@
amdgpu_irq_add_id(e1,e2,<+...
- 140
+ VISLANDS30_IV_SRCID_SYS_PAGE_INV_FAULT
...+>,e3)
@@
expression e1,e2,e3;
@@
amdgpu_irq_add_id(e1,e2,<+...
- 141
+ VISLANDS30_IV_SRCID_SYS_MEM_PROT_FAULT
...+>,e3)

@@
expression e1,e2,e3;
@@
amdgpu_irq_add_id(e1,e2,<+...
- 144
+ VISLANDS30_IV_SRCID_SEM_PAGE_INV_FAULT
...+>,e3)
@@
expression e1,e2,e3;
@@
amdgpu_irq_add_id(e1,e2,<+...
- 145
+ VISLANDS30_IV_SRCID_SEM_MEM_PROT_FAULT
...+>,e3)

@@
expression e1,e2,e3;
@@
amdgpu_irq_add_id(e1,e2,<+...
- 146
+ VISLANDS30_IV_SRCID_GFX_PAGE_INV_FAULT
...+>,e3)
@@
expression e1,e2,e3;
@@
amdgpu_irq_add_id(e1,e2,<+...
- 147
+ VISLANDS30_IV_SRCID_GFX_MEM_PROT_FAULT 
...+>,e3)

@@
expression e1,e2,e3;
@@
amdgpu_irq_add_id(e1,e2,<+...
- 162
+ VISLANDS30_IV_SRCID_ACP
...+>,e3)

@@
expression e1,e2,e3;
@@
amdgpu_irq_add_id(e1,e2,<+...
- 167
+ VISLANDS30_IV_SRCID_VCE_TRAP
...+>,e3)

@@
expression e1,e2,e3;
@@
amdgpu_irq_add_id(e1,e2,<+...
- 176
+ VISLANDS30_IV_SRCID_CP_INT_RB
...+>,e3)
@@
expression e1,e2,e3;
@@
amdgpu_irq_add_id(e1,e2,<+...
- 177
+ VISLANDS30_IV_SRCID_CP_INT_IB1 
...+>,e3)
@@
expression e1,e2,e3;
@@
amdgpu_irq_add_id(e1,e2,<+...
- 178
+ VISLANDS30_IV_SRCID_CP_INT_IB2 
...+>,e3)
@@
expression e1,e2,e3;
@@
amdgpu_irq_add_id(e1,e2,<+...
- 180
+ VISLANDS30_IV_SRCID_CP_PM4_RES_BITS_ERR
...+>,e3)
@@
expression e1,e2,e3;
@@
amdgpu_irq_add_id(e1,e2,<+...
- 181
+ VISLANDS30_IV_SRCID_CP_END_OF_PIPE 
...+>,e3)
@@
expression e1,e2,e3;
@@
amdgpu_irq_add_id(e1,e2,<+...
- 183
+ VISLANDS30_IV_SRCID_CP_BAD_OPCODE
...+>,e3)
@@
expression e1,e2,e3;
@@
amdgpu_irq_add_id(e1,e2,<+...
- 184
+ VISLANDS30_IV_SRCID_CP_PRIV_REG_FAULT
...+>,e3)
@@
expression e1,e2,e3;
@@
amdgpu_irq_add_id(e1,e2,<+...
- 185
+ VISLANDS30_IV_SRCID_CP_PRIV_INSTR_FAULT
...+>,e3)
@@
expression e1,e2,e3;
@@
amdgpu_irq_add_id(e1,e2,<+...
- 186
+ VISLANDS30_IV_SRCID_CP_WAIT_MEM_SEM_FAULT
...+>,e3)
@@
expression e1,e2,e3;
@@
amdgpu_irq_add_id(e1,e2,<+...
- 187
+ VISLANDS30_IV_SRCID_CP_GUI_IDLE
...+>,e3)
@@
expression e1,e2,e3;
@@
amdgpu_irq_add_id(e1,e2,<+...
- 188
+ VISLANDS30_IV_SRCID_CP_GUI_BUSY
...+>,e3)
@@
expression e1,e2,e3;
@@
amdgpu_irq_add_id(e1,e2,<+...
- 191
+ VISLANDS30_IV_SRCID_CP_COMPUTE_QUERY_STATUS
...+>,e3)
@@
expression e1,e2,e3;
@@
amdgpu_irq_add_id(e1,e2,<+...
- 197
+ VISLANDS30_IV_SRCID_CP_ECC_ERROR 
...+>,e3)

@@
expression e1,e2,e3;
@@
amdgpu_irq_add_id(e1,e2,<+...
- 199
+ CARRIZO_IV_SRCID_CP_COMPUTE_QUERY_STATUS 
...+>,e3)

@@
expression e1,e2,e3;
@@
amdgpu_irq_add_id(e1,e2,<+...
- 192
+ VISLANDS30_IV_SRCID_CP_WAIT_REG_MEM_POLL_TIMEOUT 
...+>,e3)
@@
expression e1,e2,e3;
@@
amdgpu_irq_add_id(e1,e2,<+...
- 193
+ VISLANDS30_IV_SRCID_CP_SEM_SIG_INCOMPL 
...+>,e3)
@@
expression e1,e2,e3;
@@
amdgpu_irq_add_id(e1,e2,<+...
- 194
+ VISLANDS30_IV_SRCID_CP_PREEMPT_ACK 
...+>,e3)
@@
expression e1,e2,e3;
@@
amdgpu_irq_add_id(e1,e2,<+...
- 195
+ VISLANDS30_IV_SRCID_CP_GENERAL_PROT_FAULT
...+>,e3)
@@
expression e1,e2,e3;
@@
amdgpu_irq_add_id(e1,e2,<+...
- 196
+ VISLANDS30_IV_SRCID_CP_GDS_ALLOC_ERROR 
...+>,e3)
@@
expression e1,e2,e3;
@@
amdgpu_irq_add_id(e1,e2,<+...
- 197
+ VISLANDS30_IV_SRCID_CP_ECC_ERROR 
...+>,e3)
@@
expression e1,e2,e3;
@@
amdgpu_irq_add_id(e1,e2,<+...
- 202
+ VISLANDS30_IV_SRCID_RLC_STRM_PERF_MONITOR
...+>,e3)

@@
expression e1,e2,e3;
@@
amdgpu_irq_add_id(e1,e2,<+...
- 218
+ VISLANDS30_IV_SDMA_ATOMIC_SRC_ID 
...+>,e3)

@@
expression e1,e2,e3;
@@
amdgpu_irq_add_id(e1,e2,<+...
- 220
+ VISLANDS30_IV_SRCID_SDMA_ECC_ERROR 
...+>,e3)

@@
expression e1,e2,e3;
@@
amdgpu_irq_add_id(e1,e2,<+...
- 224
+ VISLANDS30_IV_SRCID_SDMA_TRAP	 
...+>,e3)
@@
expression e1,e2,e3;
@@
amdgpu_irq_add_id(e1,e2,<+...
- 225
+ VISLANDS30_IV_SRCID_SDMA_SEM_INCOMPLETE
...+>,e3)
@@
expression e1,e2,e3;
@@
amdgpu_irq_add_id(e1,e2,<+...
- 226
+ VISLANDS30_IV_SRCID_SDMA_SEM_WAIT
...+>,e3)

@@
expression e1,e2,e3;
@@
amdgpu_irq_add_id(e1,e2,<+...
- 229
+ VISLANDS30_IV_SRCID_SMU_DISP_TIMER2_TRIGGER
...+>,e3)

@@
expression e1,e2,e3;
@@

amdgpu_irq_add_id(e1,e2,<+...
- 230
+ VISLANDS30_IV_SRCID_CG_TSS_THERMAL_LOW_TO_HIGH 
...+>,e3)
@@
expression e1,e2,e3;
@@

amdgpu_irq_add_id(e1,e2,<+...
- 231
+ VISLANDS30_IV_SRCID_CG_TSS_THERMAL_HIGH_TO_LOW 
...+>,e3)

@@
expression e1,e2,e3;
@@
amdgpu_irq_add_id(e1,e2,<+...
- 232
+ VISLANDS30_IV_SRCID_GRBM_READ_TIMEOUT_ERR
...+>,e3)
@@
expression e1,e2,e3;
@@
amdgpu_irq_add_id(e1,e2,<+...
- 233
+ VISLANDS30_IV_SRCID_GRBM_REG_GUI_IDLE
...+>,e3)

@@
expression e1,e2,e3;
@@
amdgpu_irq_add_id(e1,e2,<+...
- 239
+ VISLANDS30_IV_SRCID_SQ_INTERRUPT_MSG 
...+>,e3)

@@
expression e1,e2,e3;
@@
amdgpu_irq_add_id(e1,e2,<+...
- 240
+ VISLANDS30_IV_SRCID_SDMA_PREEMPT 
...+>,e3)
@@
expression e1,e2,e3;
@@
amdgpu_irq_add_id(e1,e2,<+...
- 242
+ VISLANDS30_IV_SRCID_SDMA_VM_HOLE 
...+>,e3)
@@
expression e1,e2,e3;
@@
amdgpu_irq_add_id(e1,e2,<+...
- 243
+ VISLANDS30_IV_SRCID_SDMA_CTXEMPTY
...+>,e3)
@@
expression e1,e2,e3;
@@
amdgpu_irq_add_id(e1,e2,<+...
- 244
+ VISLANDS30_IV_SRCID_SDMA_DOORBELL_INVALID
...+>,e3)
@@
expression e1,e2,e3;
@@
amdgpu_irq_add_id(e1,e2,<+...
- 245
+ VISLANDS30_IV_SRCID_SDMA_FROZEN
...+>,e3)
@@
expression e1,e2,e3;
@@
amdgpu_irq_add_id(e1,e2,<+...
- 246
+ VISLANDS30_IV_SRCID_SDMA_POLL_TIMEOUT
...+>,e3)
@@
expression e1,e2,e3;
@@
amdgpu_irq_add_id(e1,e2,<+...
- 247
+ VISLANDS30_IV_SRCID_SDMA_SRBM_WRITE
...+>,e3)

@@
expression e1,e2,e3;
@@
amdgpu_irq_add_id(e1,e2,<+...
- 248
+ VISLANDS30_IV_SRCID_CG_THERMAL_TRIG
...+>,e3)

@@
expression e1,e2,e3;
@@
amdgpu_irq_add_id(e1,e2,<+...
- 253
+ VISLANDS30_IV_SRCID_SMU_DISP_TIMER_TRIGGER 
...+>,e3)
