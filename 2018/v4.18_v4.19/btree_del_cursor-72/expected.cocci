@@
expression e,error;
@@

                xfs_btree_del_cursor(e, 
-                       (error ? XFS_BTREE_ERROR : XFS_BTREE_NOERROR)
+			error
		)

@@
expression e,error;
@@

                xfs_btree_del_cursor(e,
			 error < 0
+                        ? XFS_BTREE_ERROR : XFS_BTREE_NOERROR
		)

@@
expression error,cur;
identifier l;
@@

 	error = xfs_rmap_unmap(...);
-	if (error)
-		goto l;
 
-	xfs_btree_del_cursor(cur, XFS_BTREE_NOERROR);
-	return 0;

-l:
-	xfs_btree_del_cursor(cur, XFS_BTREE_ERROR);
+	xfs_btree_del_cursor(cur, error);
 	return error;
