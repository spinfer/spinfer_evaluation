// doesn't manage to do everything, but the rest doesn't really generalize
@@
identifier time;
@@
-       int time = get_seconds();
+       time64_t time = ktime_get_seconds();

@@
identifier time;
@@
-       time_t time = get_seconds();
+       time64_t time = ktime_get_seconds();

@@
identifier time;
@@
-       unsigned long time = get_seconds();
+       time64_t time = ktime_get_seconds();

@@
identifier time;
@@
-       int time;
+       time64_t time;
	<+...
	time =
-       get_seconds()
+       ktime_get_seconds()
	...+>

@@
identifier time;
@@
-       time_t time;
+       time64_t time;
	<+...
	time =
-       get_seconds()
+       ktime_get_seconds()
	...+>

@@
identifier time;
@@
-       unsigned long time;
+       time64_t time;
	<+...
	time =
-       get_seconds()
+       ktime_get_seconds()
	...+>

@@
expression stop_at;
typedef u32;
@@

ULONG_CMP_LT(
-            get_seconds()
+            (unsigned long) ktime_get_seconds()
           , stop_at)

@@
@@
-       get_seconds()
+       ktime_get_seconds()
