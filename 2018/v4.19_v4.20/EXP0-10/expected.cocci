// order important, first two rules must come before the last two
@@
struct platform_nand_data *pdata;
expression chip; // struct nand_chip
@@
(
pdata->ctrl.cmd_ctrl
|
- chip.cmd_ctrl
+ chip.legacy.cmd_ctrl
)

@@
expression chip; // struct nand_chip
@@
- chip.cmdfunc
+ chip.legacy.cmdfunc

@@
expression chip; // struct nand_chip *
@@
- chip->cmd_ctrl
+ chip->legacy.cmd_ctrl

@@
expression chip; // struct nand_chip *
@@
- chip->cmdfunc
+ chip->legacy.cmdfunc
