@@
identifier e1;
expression e2;
type T;
@@

(
- T e1 = to_platform_device(e2);
  ...
- platform_get_drvdata(e1)
+ dev_get_drvdata(e2)
  ... when any
&
  T e1 = to_platform_device(e2);
  <... when != e1
(
  platform_get_drvdata(e1)
|
- &e1->dev
+ e2
)
  ...>
)

@@
expression e1,e2;
@@

(
- e1 = to_platform_device(e2);
  ...
- platform_get_drvdata(e1)
+ dev_get_drvdata(e2)
  ... when any
&
  e1 = to_platform_device(e2);
  <... when != e1
(
  platform_get_drvdata(e1)
|
- &e1->dev
+ e2
)
  ...>
)
