// there are a couple cases where the change is composed with some other optimization, and one case where a change oportunity is missed.

@r@
expression node;
constant char[] str;
identifier print = {pr_err,dev_err,pr_info,pr_warn,dev_warn,pr_crit,pr_debug,panic,kasprintf,scnprintf};
@@

print(...,str, node->name,...)

@script:ocaml s@
str << r.str;
newstr;
@@

match Str.bounded_split (Str.regexp_string "%s") str 2 with
  [b;a] -> newstr := make_expr (Printf.sprintf "%s%%pOFn%s" b a)
| _ -> Coccilib.include_match false

@@
struct device_node *node;
constant char[] r.str;
expression s.newstr;
identifier r.print;
@@

  print (...,
- str, node->name
+ newstr, node
    ,...)

@@
struct device_node *node;
constant char[] r.str;
expression s.newstr;
identifier r.print;
@@

  print (...,
- str, node->parent->name
+ newstr, node->parent
    ,...)

@@
expression node;
constant char[] r.str;
expression s.newstr;
identifier r.print;
@@

  print (...,
- str, node.of_node->name
+ newstr, node.of_node
    ,...)

// ------------

@r1@
struct device_node *node;
constant char[] str;
identifier print = {pr_err,dev_err,pr_info,pr_warn,dev_warn,pr_crit,pr_debug,panic,kasprintf,scnprintf};
expression x;
@@

print(...,str, x, node->name,...)

@script:ocaml s1@
str << r1.str;
newstr;
@@

match Str.bounded_split (Str.regexp_string "%") str 3 with
  [b1;b2;a] -> newstr := make_expr (Printf.sprintf "%s%%pOFn%s" (String.concat "%" [b1;b2]) (String.sub a 1 (String.length a - 1)))
| _ -> Coccilib.include_match false

@@
struct device_node *node;
constant char[] r1.str;
expression s1.newstr;
identifier r1.print;
expression x;
@@

  print (...,
- str, x, node->name
+ newstr, x, node
    ,...)

@@
struct device_node *node;
constant char[] r1.str;
expression s1.newstr;
identifier r1.print;
expression x;
@@

  print (...,
- str, x, node->parent->name
+ newstr, x, node->parent
    ,...)

@@
expression node;
constant char[] r1.str;
expression s1.newstr;
identifier r1.print;
expression x;
@@

  print (...,
- str, x, node.of_node->name
+ newstr, x, node.of_node
    ,...)

// -------------

@r2@
struct device_node *node;
constant char[] str;
identifier print = {pr_err,dev_err,pr_info,pr_warn,dev_warn,pr_crit,pr_debug,panic,kasprintf,scnprintf};
expression x,y;
@@

print(...,str, x,y, node->name,...)

@script:ocaml s2@
str << r2.str;
newstr;
@@

match Str.bounded_split (Str.regexp_string "%") str 4 with
  [b1;b2;b3;a] -> newstr := make_expr (Printf.sprintf "%s%%pOFn%s" (String.concat "%" [b1;b2;b3]) (String.sub a 1 (String.length a - 1)))
| _ -> Coccilib.include_match false

@@
struct device_node *node;
constant char[] r2.str;
expression s2.newstr;
identifier r2.print;
expression x,y;
@@

  print (...,
- str, x,y, node->name
+ newstr, x,y, node
    ,...)

@@
struct device_node *node;
constant char[] r2.str;
expression s2.newstr;
identifier r2.print;
expression x,y;
@@

  print (...,
- str, x,y, node->parent->name
+ newstr, x,y, node->parent
    ,...)

@@
expression node;
constant char[] r2.str;
expression s2.newstr;
identifier r2.print;
expression x,y;
@@

  print (...,
- str, x,y, node.of_node->name
+ newstr, x,y, node.of_node
    ,...)
