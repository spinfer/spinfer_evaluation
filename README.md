# Spinfer Evaluation

Spinfer evaluation data for USENIX ATC 2020: https://www.usenix.org/conference/atc20/presentation/serrano

## Before running
Make sure to compile Spinfer and eval_patch before using the script.

In the Spinfer directory:
```bash
make build
make eval_patch
```

In `run_everything.py` change `SPINFER` and `EVAL_PATCH` constants for the
Spinfer and eval_patch path of your installation.
The script requires python 3.7+.


## How to run
From one of the directories, run the script with the argument `experiments_to_run.txt`.
For instance, if you want to run the challenging dataset:
```bash
cd challenging
./run_everything experiments_to_run.txt

```

The results are periodically updated in the `data_backup.json` file.

