@@
expression x;
identifier f;
constant char[] c;
statement S1,S2;
@@

x = \(kmalloc\|kcalloc\|kzalloc\|devm_kmalloc\|devm_kcalloc\|devm_kzalloc
	\|kmemdup\|kmem_cache_alloc\|kmem_cache_zalloc
	\|rhashtable_lookup_fast\)
      (...);
if (x == NULL)
- {
- f(...,c,...);
  S1
- }
 else S2

@@
expression x;
identifier f;
constant char[] c;
statement S;
@@

x = \(kmalloc\|kcalloc\|kzalloc\|devm_kmalloc\|devm_kcalloc\|devm_kzalloc
	\|kmemdup\|kmem_cache_alloc\|kmem_cache_zalloc
	\|rhashtable_lookup_fast\)
      (...);
if (x == NULL) {
  ...
- f(...,c,...);
  ...
} else S

@@
identifier f,x;
constant char[] c;
statement S1,S2;
type T;
@@

T x = \(kmalloc\|kcalloc\|kzalloc\|devm_kmalloc\|devm_kcalloc\|devm_kzalloc
	\|kmemdup\|kmem_cache_alloc\|kmem_cache_zalloc
	\|rhashtable_lookup_fast\)
      (...);
if (x == NULL)
- {
- f(...,c,...);
  S1
- }
 else S2

@@
identifier x,f;
constant char[] c;
statement S;
type T;
@@

T x = \(kmalloc\|kcalloc\|kzalloc\|devm_kmalloc\|devm_kcalloc\|devm_kzalloc
	\|kmemdup\|kmem_cache_alloc\|kmem_cache_zalloc
	\|rhashtable_lookup_fast\)
      (...);
if (x == NULL) {
  ...
- f(...,c,...);
  ...
} else S

// --------------

@@
expression x;
identifier f;
constant char[] c;
statement S1,S2;
@@

x = devm_ioremap_resource(...);
if (IS_ERR(x))
- {
- f(...,c,...);
  S1
- }
 else S2

@@
expression x;
identifier f;
constant char[] c;
statement S;
@@

x = devm_ioremap_resource(...);
if (IS_ERR(x)) {
  ...
- f(...,c,...);
  ...
} else S

@@
identifier f,x;
constant char[] c;
statement S1,S2;
type T;
@@

T x = devm_ioremap_resource(...);
if (IS_ERR(x))
- {
- f(...,c,...);
  S1
- }
 else S2

@@
identifier x,f;
constant char[] c;
statement S;
type T;
@@

T x = devm_ioremap_resource(...);
if (IS_ERR(x)) {
  ...
- f(...,c,...);
  ...
} else S
