@@
expression e1,e2,e3,r;
constant GFP_KERNEL;
@@

+	e1 = memdup_user(e2, e3);
+	if (IS_ERR(e1)) {
-	e1 = kmalloc(e3, GFP_KERNEL);
-	if (e1 == NULL) {
		...
-		return ...;
-	}

-	if (copy_from_user(e1, e2, e3))
-       {
-		kfree(e1);

-		return r;
+		return PTR_ERR(e1);
	}

@@
expression e1,e2,e3,r;
statement S;
constant GFP_KERNEL;
@@

-	e1 = kmalloc(e3, GFP_KERNEL);
-	if (e1 == NULL)
-		S

-	if (copy_from_user(e1, e2, e3))
+	e1 = memdup_user(e2, e3);
+	if (IS_ERR(e1))
-       {
-		kfree(e1);
-		return r;
+		return PTR_ERR(e1);
-	}

@@
expression e1,e2,e3,r;
statement S;
constant GFP_KERNEL;
@@

-	e1 = kmalloc(e3, GFP_KERNEL);
-	if (e1 == NULL)
-		S

-	if (copy_from_user(e1, e2, e3))
+	e1 = memdup_user(e2, e3);
+	if (IS_ERR(e1))
        {
		...
-		kfree(e1);
		...
-		return r;
+		return PTR_ERR(e1);
	}

@@
expression e1,e2,e3,r;
statement S;
constant C;
constant GFP_KERNEL;
@@

-	e1 = kmalloc(e3, GFP_KERNEL);
-	if (e1 == NULL)
-		S

-	if (copy_from_user(e1, e2, e3))
+	e1 = memdup_user(e2, e3);
+	if (IS_ERR(e1))
        {
-		r = -C;
+		r = PTR_ERR(e1);
-		kfree(e1);
		...
	}
