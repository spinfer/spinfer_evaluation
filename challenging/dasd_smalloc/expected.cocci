@@
expression e,e1,e2,e3,e4;
identifier f,req;
@@

f(...,struct request *req,...) { <...
-	e = dasd_smalloc_request(e1,e2,e3,e4);
+	e = dasd_smalloc_request(e1,e2,e3,e4,blk_mq_rq_to_pdu(req));
...> }

/*@@
expression e,e1,e2,e3,e4,req;
statement S1,S2,S3;
@@


	if (rq_data_dir(req) == READ) {
		S1
	} else if (rq_data_dir(req) == WRITE) {
		S2
	} else
		S3
	...
-	e = dasd_smalloc_request(e1,e2,e3,e4);
+	e = dasd_smalloc_request(e1,e2,e3,e4,blk_mq_rq_to_pdu(req));

@@
expression e,e1,e2,e3,e4,req,f,s;
@@


	f = blk_rq_pos(req) >> s;
	...
-	e = dasd_smalloc_request(e1,e2,e3,e4);
+	e = dasd_smalloc_request(e1,e2,e3,e4,blk_mq_rq_to_pdu(req));*/

@@
expression e,e1,e2,e3,e4;
@@

-	e = dasd_smalloc_request(e1,e2,e3,e4);
+	e = dasd_smalloc_request(e1,e2,e3,e4,NULL);
