@@
expression d,res;
@@

	return
-		devm_request_and_ioremap(d, res);
+		devm_ioremap_resource(d, res);

// the next three rules are related, but address the {} issue
@@
expression e,d,res;
statement S,S1;
@@

-	e = devm_request_and_ioremap(d, res);
+	e = devm_ioremap_resource(d, res);
	if (IS_ERR(e))
-	{
- 		dev_err(...);
		S
-	}
	else S1

@@
expression e,d,res;
statement S;
@@

-	e = devm_request_and_ioremap(d, res);
+	e = devm_ioremap_resource(d, res);
	if (IS_ERR(e)) {
		...
?- 		dev_err(...);
		...
	}
	else S

@@
expression e,d,res;
statement S,S1;
@@

-	e = devm_request_and_ioremap(d, res);
+	e = devm_ioremap_resource(d, res);
	if (IS_ERR(e)) S else S1

// break up double call
@@
expression e1,e2;
statement S;
@@

	e1 = devm_request_and_ioremap(...);
+	if (!e1) S
	e2 = devm_request_and_ioremap(...);
	if (
-            !e1 ||
             !e2) S

@@
expression e,d,res;
local idexpression r;
identifier l;
constant C;
statement S;
@@

-	e = devm_request_and_ioremap(d, res);
+	e = devm_ioremap_resource(d, res);
	if (
-	    e == NULL
+	    IS_ERR(e)
            || ... )
	{
		...
?- 		dev_err(...);
		...
-		r = C;
+		r = PTR_ERR(e);
		...
?- 		dev_err(...);
		...
		goto l;
	}
	else S

// the next three rules are related, but address the {} issue

@@
expression e,d,res,r;
statement S;
@@

-	e = devm_request_and_ioremap(d, res);
+	e = devm_ioremap_resource(d, res);
        if (
-	    e == NULL
+	    IS_ERR(e)
            || ...)
-	{
- 		dev_err(...);
		return
-			r
+			PTR_ERR(e)
		;
-	}
	else S

@@
expression e,d,res,r;
statement S;
@@

-	e = devm_request_and_ioremap(d, res);
+	e = devm_ioremap_resource(d, res);
        if (
-	    e == NULL
+	    IS_ERR(e)
            || ...)
	{
		...
- 		dev_err(...);
		...
		return
-			r
+			PTR_ERR(e)
		;
	} else S

@@
expression e,d,res,r;
statement S;
@@

-	e = devm_request_and_ioremap(d, res);
+	e = devm_ioremap_resource(d, res);
	if (
-	    e == NULL
+	    IS_ERR(e)
	    || ...)
	{
		...
		return
-			r
+			PTR_ERR(e)
		;
	} else S

@@
expression e,d,res;
statement S1,S2; // no return or goto
@@

-	e = devm_request_and_ioremap(d, res);
+	e = devm_ioremap_resource(d, res);
	if (
-	    e == NULL
+	    IS_ERR(e)
	    || ...)
	S1 else S2

@@
expression e,d,res; // no test at all
statement S1,S2;
@@

-	e = devm_request_and_ioremap(d, res);
+	e = devm_ioremap_resource(d, res);
+	if (IS_ERR(e)) return PTR_ERR(e);
	... when != if (e == NULL || ...) S1 else S2
            when != if (IS_ERR(e) || ...) S1 else S2