@@
expression e1,e2,tp;
@@

-		xfs_defer_init(e1, e2);
-		(tp)->t_dfops = e1;
+		xfs_defer_init(tp, e1, e2);

@@
expression e1,e2,tp;
@@

-		xfs_defer_init(e1, e2);
+		xfs_defer_init(NULL, e1, e2);
