@@
identifier f,dev;
expression e;
@@

f(...,struct drm_device *dev,...) {
  ...
- dma_buf_export(e)
+ drm_gem_dmabuf_export(dev, e)
  ...
}
