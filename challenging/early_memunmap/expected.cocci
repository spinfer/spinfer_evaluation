@@
expression phys_addr, addr, size, e;
@@
  addr = early_memremap(phys_addr, size)
  ... when != addr = e
- early_iounmap(addr, size)
+ early_memunmap(addr, size)
