@@
expression e1,e2,e3,gpio,ret;
statement S;
@@

-	gpio = devm_gpiod_get_index(e1, e2, e3);
+	gpio = devm_gpiod_get_index(e1, e2, e3, GPIOD_IN);
	...
-	ret = gpiod_direction_input(gpio);
-	if (\(ret\|ret < 0\)) S

@@
expression e1,e2,e3,gpio;
@@

-	gpio = devm_gpiod_get_index(e1, e2, e3);
+	gpio = devm_gpiod_get_index(e1, e2, e3, GPIOD_IN);
	...
-	return gpiod_direction_input(gpio);
+	return 0;

@@
expression e1,e2,gpio,ret;
statement S;
@@

-	gpio = devm_gpiod_get(e1, e2);
+	gpio = devm_gpiod_get(e1, e2, GPIOD_IN);
	...
-	ret = gpiod_direction_input(gpio);
-	if (\(ret\|ret < 0\)) S

@@
expression e1,e2,e3,gpio,ret;
statement S;
@@

-	gpio = devm_gpiod_get_index(e1, e2, e3);
+	gpio = devm_gpiod_get_index(e1, e2, e3, GPIOD_OUT_LOW);
	...
-	ret = gpiod_direction_output(gpio, 0);
-	if (\(ret\|ret < 0\)) S

@@
expression e1,e2,gpio,ret;
statement S;
@@

-	gpio = devm_gpiod_get(e1, e2);
+	gpio = devm_gpiod_get(e1, e2, GPIOD_OUT_LOW);
	...
-	ret = gpiod_direction_output(gpio, 0);
-	if (\(ret\|ret < 0\)) S

// ----------------

@@
expression e1,e2,e3,gpio;
@@

-	gpio = devm_gpiod_get_index(e1, e2, e3);
+	gpio = devm_gpiod_get_index(e1, e2, e3, GPIOD_IN);
	...
-	gpiod_direction_input(gpio);

@@
expression e1,e2,gpio;
@@

-	gpio = devm_gpiod_get(e1, e2);
+	gpio = devm_gpiod_get(e1, e2, GPIOD_IN);
	...
-	gpiod_direction_input(gpio);

@@
expression e1,e2,e3,gpio;
@@

-	gpio = devm_gpiod_get_index(e1, e2, e3);
+	gpio = devm_gpiod_get_index(e1, e2, e3, GPIOD_OUT_LOW);
	...
-	gpiod_direction_output(gpio, 0);

@@
expression e1,e2,gpio;
@@

-	gpio = devm_gpiod_get(e1, e2);
+	gpio = devm_gpiod_get(e1, e2, GPIOD_OUT_LOW);
	...
-	gpiod_direction_output(gpio, 0);

// ----------------

@@
expression e,e1,e2,e3,gpio;
@@

-	gpio = devm_gpiod_get_index(e1, e2, e3);
+	gpio = devm_gpiod_get_index(e1, e2, e3, GPIOD_IN);
	...
	e = gpiod_to_irq(gpio)

@@
expression e,e1,e2,gpio;
@@

-	gpio = devm_gpiod_get(e1, e2);
+	gpio = devm_gpiod_get(e1, e2, GPIOD_IN);
	...
	e = gpiod_to_irq(gpio)

@@
expression e,e1,e2,gpio;
@@

-	gpio = devm_gpiod_get_optional(e1, e2);
+	gpio = devm_gpiod_get_optional(e1, e2, GPIOD_IN);
	...
	e = gpiod_to_irq(gpio)

// ----------------

@@
identifier ret;
@@

(
- int ret;
  ... when exists
  devm_gpiod_get(...)
  ... when any
&
  int ret;
  ... when != ret
      when strict
)

@@
identifier ret;
@@

(
- int ret;
  ... when exists
  devm_gpiod_get_index(...)
  ... when any
&
  int ret;
  ... when != ret
      when strict
)
