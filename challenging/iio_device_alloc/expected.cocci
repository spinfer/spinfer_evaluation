@@
expression e,e1,e2,ret;
identifier l1,l2,dev;
@@
	struct device *dev;
	... when != l2
(
-       e = iio_device_alloc(e1);
+       e = devm_iio_device_alloc(dev, e1);
        if (e == NULL)
(
-	{
-               ret = e2;
-               goto l2;
+               return e2;
-       }
|
	{
		...
-               ret = e2;
		...
-               goto l2;
+               return e2;
       }
)
<...
-	goto l1;
+	return ret;
...>
&
e = iio_device_alloc(e1);
... when exists
-l1:
-       iio_device_free(e);
-l2:
        return ret;
)

@@
expression e,e1,e2,ret;
identifier l1,l2,x,f;
type T;
@@
f(T x) {
	... when != l2
(
-       e = iio_device_alloc(e1);
+       e = devm_iio_device_alloc(dev, e1);
        if (e == NULL)
(
-	{
-               ret = e2;
-               goto l2;
+               return e2;
-       }
|
	{
		...
-               ret = e2;
		...
-               goto l2;
+               return e2;
       }
)
<...
-	goto l1;
+	return ret;
...>
&
e = iio_device_alloc(e1);
... when exists
-l1:
-       iio_device_free(e);
-l2:
        return ret;
)
}

// ------------------------------------

@@
expression e,e1,e2,ret;
identifier l1,dev;
@@

	struct device *dev;
	...
(
-       e = iio_device_alloc(e1);
+       e = devm_iio_device_alloc(dev, e1);
        if (e == NULL) {
	       ...
               return e2;
        }
<...
-	goto l1;
+	return ret;
...>
&
e = iio_device_alloc(e1);
... when exists
-l1:
-       iio_device_free(e);
        return ret;
)

@@
expression e,e1,e2,ret;
identifier l1,x,f;
type T;
@@

f(T x) {
	...
(
-       e = iio_device_alloc(e1);
+       e = devm_iio_device_alloc(&x->dev, e1);
        if (e == NULL) {
	       ...
               return e2;
        }
<...
-	goto l1;
+	return ret;
...>
&
e = iio_device_alloc(e1);
... when exists
-l1:
-       iio_device_free(e);
        return ret;
)
}