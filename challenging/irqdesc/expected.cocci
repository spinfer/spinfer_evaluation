@@
typedef u32;
type T = {unsigned int, u32};
identifier f, irq, desc;
@@

  f(T irq, struct irq_desc *desc) {
<+...
- irq_get_chip(irq)
+ irq_desc_get_chip(desc)
...+>
  }


@@
typedef u32;
type T = {unsigned int, u32};
identifier f, irq, desc;
@@

  f(T irq, struct irq_desc *desc) {
<+...
- irq_get_handler_data(irq)
+ irq_desc_get_handler_data(desc)
...+>
  }


@@
typedef u32;
type T = {unsigned int, u32};
identifier f, irq, desc;
@@

  f(T irq, struct irq_desc *desc) {
<+...
- irq_get_irq_data(irq)
+ irq_desc_get_irq_data(desc)
...+>
  }


@@
typedef u32;
type T = {unsigned int, u32};
identifier f, irq, desc;
@@

  f(T irq, struct irq_desc *desc) {
<+...
- irq_get_chip_data(irq)
+ irq_desc_get_chip_data(desc)
...+>
  }


@@
identifier f, data;
@@

  f(struct irq_data *data) {
<+...
- irq_get_chip_data(data->irq)
+ irq_data_get_irq_chip_data(data)
...+>
  }
