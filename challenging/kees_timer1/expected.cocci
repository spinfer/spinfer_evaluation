@@
expression t,d,f,e1,e2,e3;
@@

-               init_timer(&t);
+               setup_timer(&t,f,d);
... when != d = e1
    when != f = e2
-               t.data = d;
... when != f = e3
-               t.function = f;

@@
expression t,d,f,e1,e2,e3;
@@

-               init_timer(&t);
+               setup_timer(&t,f,d);
... when != d = e1
    when != f = e2
-               t.function = f;
... when != f = e3
-               t.data = d;

@@
expression t,d,f,e1,e2,e3;
@@

-               t.data = d;
... when != d = e3
-               t.function = f;
... when != d = e1
    when != f = e2
-               init_timer(&t);
+               setup_timer(&t,f,d);

@@
expression t,d,f,e1,e2,e3;
@@

-               t.function = f;
... when != f = e3
-               t.data = d;
... when != d = e1
    when != f = e2
-               init_timer(&t);
+               setup_timer(&t,f,d);

// --------------------------

@@
expression t,d,f,e3;
@@

-               init_timer(&t);
+               setup_timer(&t,f,0UL);
... when != f = e3
-               t.function = f;

@@
expression t,d,f,e3;
@@

-               t.function = f;
... when != f = e3
-               init_timer(&t);
+               setup_timer(&t,f,0UL);
