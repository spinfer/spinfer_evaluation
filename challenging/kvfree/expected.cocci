@@
expression e;
@@

-       if (e != NULL && is_vmalloc_addr(e))
-               vfree(e);
-       else
-               kfree(e);
+       kvfree(e);

@@
expression e;
@@

- if (e != NULL)
-       {
-       if (is_vmalloc_addr(e))
-               vfree(e);
-       else
-               kfree(e);
+       kvfree(e);
-       }

@@
expression e;
@@

-       if (is_vmalloc_addr(e))
-               vfree(e);
-       else
-               kfree(e);
+       kvfree(e);
