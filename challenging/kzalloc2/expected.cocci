@@
expression e1,e2;
statement S;
identifier e3,print1,print2;
constant char[] c1;
constant char[] c2;
type T;
@@

	T e3 =
-       kmalloc
+       kzalloc
	   (e1, e2);
        <... when != e3
(
	print1(...,c1,...,e3,...);
|
	kfree(e3);
)
        ...>
        if (e3 == NULL || ...) S
        <... when != e3
(
	print2(...,c2,...,e3,...);
|
	kfree(e3);
)
        ...>
-       memset(e3, 0, e1);

@@
expression e1,e2,e3;
identifier print1,print2;
constant char[] c1;
constant char[] c2;
statement S;
@@

-       e3 = kmalloc(e1, e2)
+       e3 = kzalloc(e1, e2)
        <... when != e3
(
	print1(...,c1,...,e3,...);
|
	kfree(e3);
)
       ...>
        if (e3 == NULL || ...) S
        <... when != e3
(
	print2(...,c2,...,e3,...);
|
	kfree(e3);
)
        ...>
-       memset(e3, 0, e1);

@@
expression e1,e2;
identifier e3,print1,print2;
constant char[] c1;
constant char[] c2;
type T;
@@

	T e3 =
-       kmalloc
+       kzalloc
	   (e1, e2);
        <... when != e3
(
	print1(...,c1,...,e3,...);
|
	kfree(e3);
)
        ...>
        BUG_ON (e3 == NULL);
        <... when != e3
(
	print2(...,c2,...,e3,...);
|
	kfree(e3);
)
        ...>
-       memset(e3, 0, e1);

@@
expression e1,e2,e3;
identifier print1,print2;
constant char[] c1;
constant char[] c2;
@@

-       e3 = kmalloc(e1, e2)
+       e3 = kzalloc(e1, e2)
        <... when != e3
(
	print1(...,c1,...,e3,...);
|
	kfree(e3);
)
       ...>
        BUG_ON (e3 == NULL);
        <... when != e3
(
	print2(...,c2,...,e3,...);
|
	kfree(e3);
)
        ...>
-       memset(e3, 0, e1);

@@
expression e1,e2,e3;
identifier print;
constant char[] c;
@@

-       e3 = kmalloc(e1, e2)
+       e3 = kzalloc(e1, e2)
        <... when != e3
(
	print(...,c,...,e3,...);
|
	kfree(e3);
)
        ...>
-       memset(e3, 0, e1);

// ---------------------------------
// rename

@@
expression e1,e2,r;
statement S;
identifier e3,print1,print2,print3;
constant char[] c1;
constant char[] c2;
constant char[] c3;
type T;
@@

	T e3 =
-       kmalloc
+       kzalloc
	   (e1, e2);
        <... when != e3
(
	print1(...,c1,...,e3,...);
|
	kfree(e3);
)
        ...>
        if (e3 == NULL || ...) S
        <... when != e3
(
	print2(...,c2,...,e3,...);
|
	kfree(e3);
)
        ...>
(
	r = e3;
|
	dev_set_drvdata(r,e3);
)
	<... when != e3
	     when != r
(
	print3(...,c3,...,e3,...);
|
	kfree(e3);
)
        ...>
-       memset(e3, 0, e1);

@@
expression e1,e2,e3,r;
identifier print1,print2,print3;
constant char[] c1;
constant char[] c2;
constant char[] c3;
statement S;
@@

-       e3 = kmalloc(e1, e2)
+       e3 = kzalloc(e1, e2)
        <... when != e3
(
	print1(...,c1,...,e3,...);
|
	kfree(e3);
)
        ...>
        if (e3 == NULL || ...) S
        <... when != e3
(
	print2(...,c2,...,e3,...);
|
	kfree(e3);
)
        ...>
(
	r = e3;
|
	dev_set_drvdata(r,e3);
)
	<... when != e3
	     when != r
(
	print3(...,c3,...,e3,...);
|
	kfree(e3);
)
        ...>
-       memset(e3, 0, e1);

@@
expression e1,e2,r;
identifier e3,print1,print2,print3;
constant char[] c1;
constant char[] c2;
constant char[] c3;
type T;
@@

	T e3 =
-       kmalloc
+       kzalloc
	   (e1, e2);
        <... when != e3
(
	print1(...,c1,...,e3,...);
|
	kfree(e3);
)
        ...>
        BUG_ON (e3 == NULL);
        <... when != e3
(
	print2(...,c2,...,e3,...);
|
	kfree(e3);
)
        ...>
(
	r = e3;
|
	dev_set_drvdata(r,e3);
)
	<... when != e3
	     when != r
(
	print3(...,c3,...,e3,...);
|
	kfree(e3);
)
        ...>
-       memset(e3, 0, e1);

@@
expression e1,e2,e3,r;
identifier print1,print2,print3;
constant char[] c1;
constant char[] c2;
constant char[] c3;
@@

-       e3 = kmalloc(e1, e2)
+       e3 = kzalloc(e1, e2)
        <... when != e3
(
	print1(...,c1,...,e3,...);
|
	kfree(e3);
)
        ...>
        BUG_ON (e3 == NULL);
        <... when != e3
(
	print2(...,c2,...,e3,...);
|
	kfree(e3);
)
        ...>
(
	r = e3;
|
	dev_set_drvdata(r,e3);
)
	<... when != e3
	     when != r
(
	print3(...,c3,...,e3,...);
|
	kfree(e3);
)
        ...>
-       memset(e3, 0, e1);

@@
expression e1,e2,e3,r;
identifier print1,print2;
constant char[] c1;
constant char[] c2;
@@

-       e3 = kmalloc(e1, e2)
+       e3 = kzalloc(e1, e2)
        <... when != e3
(
	print1(...,c1,...,e3,...);
|
	kfree(e3);
)
        ...>
(
	r = e3;
|
	dev_set_drvdata(r,e3);
)
	<... when != e3
	     when != r
(
	print2(...,c2,...,e3,...);
|
	kfree(e3);
)
        ...>
-       memset(e3, 0, e1);
