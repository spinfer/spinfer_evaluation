@@
expression e,e1,e2,e3;
statement S;
@@

e =
- ldebugfs_register
+ debugfs_create_dir
     (e1,e2
-     ,NULL,e3
     )
;
-if (IS_ERR(e)) S

@@
expression e,e1,e2,e3;
statement S;
@@

e =
- ldebugfs_register
+ debugfs_create_dir
     (e1,e2
-     ,NULL,e3
     )
;
-if (IS_ERR_OR_NULL(e)) S

@@
expression e,e1,e2;
statement S;
@@

// cleanup rule because the affected code can occur in a branch

if (...)
- {
  e = debugfs_create_dir(e1,e2);
- }
  else
- {
  S
- }

@@
expression e,e1,e2,e3;
@@

e =
- ldebugfs_register
+ debugfs_create_dir
     (e1,e2
-     ,NULL,e3
     )
;
-return PTR_ERR_OR_ZERO(e);
+return 0;

@@
expression e1,e2,e3;
@@

- ldebugfs_register
+ debugfs_create_dir
     (e1,e2
-     ,NULL,e3
     )
