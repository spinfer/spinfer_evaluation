@a0@
identifier p,s;
expression e;
type T,T1;
identifier lst;
iterator name list_for_each, list_for_each_safe, list_for_each_entry;
statement S;
@@

	struct list_head *p;
+	T1 s;
	<+...
(
-       list_for_each(p, e)
+       list_for_each_entry(s, e, lst)
-	{
-               T1 s;
-               s = list_entry(p, T, lst);
		S
-	}
|
-       list_for_each(p, e)
+       list_for_each_entry(s, e, lst)
-	{
-               T1 s = list_entry(p, T, lst);
		S
-	}
|
-       list_for_each(p, e)
+       list_for_each_entry(s, e, lst)
	{
		... when != p
-               T1 s;
		... when != p
-               s = list_entry(p, T, lst);
		... when != p
	}
|
-       list_for_each(p, e)
+       list_for_each_entry(s, e, lst)
	{
		... when != p
-               T1 s = list_entry(p, T, lst);
		... when != p
	}
)
	...+>

@a1@
identifier p;
expression s,e;
type T;
identifier lst;
iterator name list_for_each, list_for_each_entry;
statement S;
@@

(
-       list_for_each(p, e)
+       list_for_each_entry(s, e, lst)
-	{
-               s = list_entry(p, T, lst);
		S
-	}
|
-       list_for_each(p, e)
+       list_for_each_entry(s, e, lst)
	{
		... when != p
-               s = list_entry(p, T, lst);
		... when != p
	}
)

// ---------------------------------------------------

@r1@
identifier p,s,n;
expression e;
type T,T1;
identifier lst;
iterator name list_for_each_safe, list_for_each_entry_safe;
statement S;
fresh identifier tmp = "tmp";
@@

	struct list_head *p;
+	T1 s;
+	T1 tmp;
	<+...
(
-       list_for_each_safe(p, n, e)
+       list_for_each_entry_safe(s, tmp, e, lst)
-	{
-               T1 s;
-               s = list_entry(p, T, lst);
		S
-	}
|
-       list_for_each_safe(p, n, e)
+       list_for_each_entry_safe(s, tmp, e, lst)
-	{
-               T1 s = list_entry(p, T, lst);
		S
-	}
|
-       list_for_each_safe(p, n, e)
+       list_for_each_entry_safe(s, tmp, e, lst)
	{
		... when != p
-               T1 s;
		... when != p
-               s = list_entry(p, T, lst);
		... when != p
	}
|
-       list_for_each_safe(p, n, e)
+       list_for_each_entry_safe(s, tmp, e, lst)
	{
		... when != p
-               T1 s = list_entry(p, T, lst);
		... when != p
	}
)
	...+>

@r2@
identifier p,n;
expression s,e;
type T;
identifier lst;
iterator name list_for_each_safe, list_for_each_entry_safe;
statement S;
fresh identifier tmpa = "tmpa";
@@

(
-       list_for_each_safe(p, n, e)
+       list_for_each_entry_safe(s, tmpa, e, lst)
-	{
-               s = list_entry(p, T, lst);
		S
-	}
|
-       list_for_each_safe(p, n, e)
+       list_for_each_entry_safe(s, tmpa, e, lst)
	{
		... when != p
-               s = list_entry(p, T, lst);
		... when != p
	}
|
	s = list_entry(p, T, lst);
)

// ---------------------------------------------------

@a00@
identifier p,s;
expression e;
type T,T1;
identifier lst;
iterator name hlist_for_each, hlist_for_each_safe, hlist_for_each_entry;
statement S;
@@

	struct hlist_node *p;
+	T1 s;
	<+...
(
-       hlist_for_each(p, e)
+       hlist_for_each_entry(s, e, lst)
-	{
-               T1 s;
-               s = hlist_entry(p, T, lst);
		S
-	}
|
-       hlist_for_each(p, e)
+       hlist_for_each_entry(s, e, lst)
-	{
-               T1 s = hlist_entry(p, T, lst);
		S
-	}
|
-       hlist_for_each(p, e)
+       hlist_for_each_entry(s, e, lst)
	{
		... when != p
-               T1 s;
		... when != p
-               s = hlist_entry(p, T, lst);
		... when != p
	}
|
-       hlist_for_each(p, e)
+       hlist_for_each_entry(s, e, lst)
	{
		... when != p
-               T1 s = hlist_entry(p, T, lst);
		... when != p
	}
)
	...+>

@a11@
identifier p;
expression s,e;
type T;
identifier lst;
iterator name hlist_for_each, hlist_for_each_entry;
statement S;
@@

(
-       hlist_for_each(p, e)
+       hlist_for_each_entry(s, e, lst)
-	{
-               s = hlist_entry(p, T, lst);
		S
-	}
|
-       hlist_for_each(p, e)
+       hlist_for_each_entry(s, e, lst)
	{
		... when != p
-               s = hlist_entry(p, T, lst);
		... when != p
	}
)

// ---------------------------------------------------

@r11@
identifier p,s,n;
expression e;
type T,T1;
identifier lst;
iterator name hlist_for_each_safe, hlist_for_each_entry_safe;
statement S;
fresh identifier tmp = "tmp";
@@

	struct hlist_node *p;
+	T1 s;
+	T1 tmp;
	<+...
(
-       hlist_for_each_safe(p, n, e)
+       hlist_for_each_entry_safe(s, tmp, e, lst)
-	{
-               T1 s;
-               s = hlist_entry(p, T, lst);
		S
-	}
|
-       hlist_for_each_safe(p, n, e)
+       hlist_for_each_entry_safe(s, tmp, e, lst)
-	{
-               T1 s = hlist_entry(p, T, lst);
		S
-	}
|
-       hlist_for_each_safe(p, n, e)
+       hlist_for_each_entry_safe(s, tmp, e, lst)
	{
		... when != p
-               T1 s;
		... when != p
-               s = hlist_entry(p, T, lst);
		... when != p
	}
|
-       hlist_for_each_safe(p, n, e)
+       hlist_for_each_entry_safe(s, tmp, e, lst)
	{
		... when != p
-               T1 s = hlist_entry(p, T, lst);
		... when != p
	}
)
	...+>

@r22@
identifier p,n;
expression s,e;
type T;
identifier lst;
iterator name hlist_for_each_safe, hlist_for_each_entry_safe;
statement S;
fresh identifier tmpa = "tmpa";
@@

(
-       hlist_for_each_safe(p, n, e)
+       hlist_for_each_entry_safe(s, tmpa, e, lst)
-	{
-               s = hlist_entry(p, T, lst);
		S
-	}
|
-       hlist_for_each_safe(p, n, e)
+       hlist_for_each_entry_safe(s, tmpa, e, lst)
	{
		... when != p
-               s = hlist_entry(p, T, lst);
		... when != p
	}
|
	s = hlist_entry(p, T, lst);
)

// not really safe; should have checked before if p was not used otherwise

@@
identifier a0.p;
@@
-	struct list_head *p;
	... when != p

@@
identifier a1.p;
@@
-	struct list_head *p;
	... when != p

@@
identifier a00.p;
@@
-	struct hlist_node *p;
	... when != p


@@
identifier a11.p;
@@
-	struct hlist_node *p;
	... when != p

@@
identifier r1.n;
@@

-	struct list_head *n;
	... when != n

@@
identifier r1.p;
@@

-	struct list_head *p;
	... when != p

@@
identifier r2.n;
@@

-	struct list_head *n;
	... when != n

@@
identifier r2.p;
@@

-	struct list_head *p;
	... when != p

@@
identifier r11.n;
@@

-	struct hlist_node *n;
	... when != n

@@
identifier r11.p;
@@

-	struct hlist_node *p;
	... when != p

@@
identifier r22.n;
@@

-	struct hlist_node *n;
	... when != n

@@
identifier r22.p;
@@

-	struct hlist_node *p;
	... when != p
