@@
expression nvmem,e,e1,pdev;
identifier d;
@@

	struct device *d = &pdev->dev;
	... when exists
-	nvmem = nvmem_register(e);
-	if (IS_ERR(nvmem))
-		return PTR_ERR(nvmem);
	...
-	platform_set_drvdata(pdev, e1);
	...
-	return 0;
+	nvmem = devm_nvmem_register(d, e);
+	return PTR_ERR_OR_ZERO(nvmem);

@@
expression nvmem,e,e1,pdev;
identifier d;
@@

	struct device *d = &pdev->dev;
	... when exists // needed here due to EHC in else
-	nvmem = nvmem_register(e);
+	nvmem = devm_nvmem_register(d, e);
	if (IS_ERR(nvmem)) { ... }
	...
-	platform_set_drvdata(pdev, e1);

@@
expression nvmem,e,e1,pdev;
@@

-	nvmem = nvmem_register(e);
-	if (IS_ERR(nvmem))
-		return PTR_ERR(nvmem);
	...
-	platform_set_drvdata(pdev, e1);
	...
-	return 0;
+	nvmem = devm_nvmem_register(&pdev->dev, e);
+	return PTR_ERR_OR_ZERO(nvmem);

@@
expression nvmem,e,e1,pdev;
@@

-	nvmem = nvmem_register(e);
+	nvmem = devm_nvmem_register(&pdev->dev, e);
	if (IS_ERR(nvmem)) { ... }
	...
-	platform_set_drvdata(pdev, e1);

@r@
identifier f,i;
@@

 struct platform_driver i = {
 	.remove	= f,
 };

@s@
identifier r.f,pdev,i;
type T;
@@

-f(struct platform_device *pdev)
-{
-	T i = platform_get_drvdata(pdev);
-
-	return nvmem_unregister(<+...i...+>);
-}

@depends on s@
identifier r.f,r.i;
@@

 struct platform_driver i = {
- 	.remove	= f,
 };
