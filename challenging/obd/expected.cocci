@@
expression e;
statement S1,S2;
@@

-	OBD_ALLOC_PTR(e);
+	e = kzalloc(sizeof(*e), GFP_NOFS);
 	if (unlikely(
-	    e == NULL
+	    !e
	   ))
	S1 else S2

@@
expression e;
statement S1,S2;
@@

-	OBD_ALLOC_PTR(e);
+	e = kzalloc(sizeof(*e), GFP_NOFS);
 	if (likely(
-	    e != NULL
+	    e
	   ))
	S1 else S2

// -------------------------------------------------------------

@@
expression e,sz;
statement S1,S2;
@@

-	OBD_ALLOC(e,sz);
+	e = kzalloc(sz, GFP_NOFS);
	if (unlikely(
-	    e == NULL
+	    !e
	   ))
	S1 else S2

@@
expression e,sz;
statement S1,S2;
@@

-	OBD_ALLOC(e,sz);
+	e = kzalloc(sz, GFP_NOFS);
	if (likely(
-	    e != NULL
+	    e
	   ))
	S1 else S2

// -------------------------------------------------------------

@@
expression e,sz;
statement S1,S2;
@@

-	OBD_ALLOC_WAIT(e,sz);
+	e = kzalloc(sz, GFP_KERNEL);
	if (unlikely(
-	    e == NULL
+	    !e
	   ))
	S1 else S2

@@
expression e,sz;
statement S1,S2;
@@

-	OBD_ALLOC_WAIT(e,sz);
+	e = kzalloc(sz, GFP_KERNEL);
	if (likely(
-	    e != NULL
+	    e
	   ))
	S1 else S2

// -------------------------------------------------------------

@@
expression e;
@@

-	OBD_ALLOC_PTR(e);
+	e = kzalloc(sizeof(*e), GFP_NOFS);

@@
expression e,sz;
@@

-	OBD_ALLOC(e,sz);
+	e = kzalloc(sz, GFP_NOFS);

@@
expression e,sz;
@@

-	OBD_ALLOC_WAIT(e,sz);
+	e = kzalloc(sz, GFP_KERNEL);
