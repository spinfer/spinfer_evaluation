@@
expression e1,e2,e3,e4,pdev;
statement S;
@@

-	e1 = platform_get_resource(pdev, IORESOURCE_MEM, e3);
-	if (e1 == NULL)
-		S
	... when any
            when != e1
+	e1 = platform_get_resource(pdev, IORESOURCE_MEM, e3);
 	e2 = devm_ioremap_resource(e4, e1);

@@
expression e1,e2,e3,e4,e5,e6,pdev;
statement S1,S2;
@@

-	e1 = platform_get_resource(pdev, IORESOURCE_MEM, e3);
-	if (e1 == NULL)
-		S1
	... when any
            when != e1
-	e5 = e1->start;
-	e6 = resource_size(e1);
	... when any
            when != e1
+	e1 = platform_get_resource(pdev, IORESOURCE_MEM, e3);
 	e2 = devm_ioremap_resource(e4, e1);
	if (...) S2
+	e5 = e1->start;
+	e6 = resource_size(e1);

@@
expression e1,e2,e3,e4,e5,e6,pdev;
statement S1,S2;
@@

-	e1 = platform_get_resource(pdev, IORESOURCE_MEM, e3);
-	if (e1 == NULL)
-		S1
	... when any
            when != e1
-	e5 = pdev->resource[e3].start;
-	e6 = resource_size(e1);
	... when any
            when != e1
+	e1 = platform_get_resource(pdev, IORESOURCE_MEM, e3);
 	e2 = devm_ioremap_resource(e4, e1);
	if (...) S2
+	e5 = pdev->resource[e3].start;
+	e6 = resource_size(e1);

@@
expression e1,e2,e3,e4,e5,pdev;
statement S1,S2;
@@

-	e1 = platform_get_resource(pdev, IORESOURCE_MEM, e3);
-	if (e1 == NULL)
-		S1
	... when any
            when != e1
-	e5 = e1->start;
	... when any
            when != e1
+	e1 = platform_get_resource(pdev, IORESOURCE_MEM, e3);
 	e2 = devm_ioremap_resource(e4, e1);
	if (...) S2
+	e5 = e1->start;

@@
expression e1,e2,e3,e4,e5,pdev;
statement S1,S2;
@@

-	e1 = platform_get_resource(pdev, IORESOURCE_MEM, e3);
-	if (e1 == NULL)
-		S1
	... when any
            when != e1
-	e5 = pdev->resource[e3].start;
	... when any
            when != e1
+	e1 = platform_get_resource(pdev, IORESOURCE_MEM, e3);
 	e2 = devm_ioremap_resource(e4, e1);
	if (...) S2
+	e5 = pdev->resource[e3].start;

@@
expression e1,e2,e3,e4,e6,pdev;
statement S1,S2;
@@

-	e1 = platform_get_resource(pdev, IORESOURCE_MEM, e3);
-	if (e1 == NULL)
-		S1
	... when any
            when != e1
-	e6 = resource_size(e1);
	... when any
            when != e1
+	e1 = platform_get_resource(pdev, IORESOURCE_MEM, e3);
 	e2 = devm_ioremap_resource(e4, e1);
	if (...) S2
+	e6 = resource_size(e1);
