@@
expression net,e1,e2,e3;
@@

- proc_net_fops_create(&net, e1, e2, e3)
+ proc_create(e1, e2, net.proc_net, e3)

@@
expression net,e1,e2,e3;
@@

- proc_net_fops_create(net, e1, e2, e3)
+ proc_create(e1, e2, net->proc_net, e3)
