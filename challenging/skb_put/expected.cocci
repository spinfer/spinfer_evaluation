@@
expression skb,len,req;
type T;
@@

	req = (T)
-	__skb_put
+	__skb_put_zero
	  (skb, len);
	...
-	memset(req, 0, len);

@@
expression skb,req;
type T;
@@

	req = (T)
-	__skb_put
+	__skb_put_zero
	  (skb, sizeof(*req));
	...
-	memset(req, 0, sizeof *req);

@@
expression skb,len,req;
type T;
@@

	req = (T)
-	skb_put
+	skb_put_zero
	  (skb, len);
	...
-	memset(req, 0, len);

@@
expression skb;
expression req;
type T;
@@

	req = (T)
-	skb_put
+	skb_put_zero
	  (skb, sizeof(*req));
	...
-	memset(req, 0, sizeof *req);
