@@
identifier hpriv;
expression ap;
@@

	struct ahci_host_priv *hpriv = ap->host->private_data;
	...
?-	ahci_stop_engine(ap);
+	hpriv->stop_engine(ap);

@@
symbol hpriv;
expression ap;
@@

-	ahci_stop_engine(ap);
+	struct ahci_host_priv *hpriv = ap->host->private_data;
+	hpriv->stop_engine(ap);


@@
identifier hpriv;
expression ap,x;
@@

	struct ahci_host_priv *hpriv = ap->host->private_data;
	...
?-	x = ahci_stop_engine(ap);
+	x = hpriv->stop_engine(ap);

@@
symbol hpriv;
expression ap,x;
@@

-	x = ahci_stop_engine(ap);
+	struct ahci_host_priv *hpriv = ap->host->private_data;
+	x = hpriv->stop_engine(ap);
