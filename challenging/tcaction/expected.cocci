@@
identifier actions;
symbol i;
expression a,exts;
statement S;
iterator name tcf_exts_for_each_action,list_for_each_entry;
@@

-	LIST_HEAD(actions);
+	int i;
	...
-       tcf_exts_to_list(exts, &actions);
-       list_for_each_entry(a, &actions, list)
+       tcf_exts_for_each_action(i, a, exts)
		S

@@
identifier actions;
expression a,exts;
@@

-	LIST_HEAD(actions);
	...
-       tcf_exts_to_list(exts, &actions);
-       a = list_first_entry(&actions, struct tc_action, list);
+       a = tcf_exts_first_action(exts);
