@@
expression port,e;
@@

// This occurs in a lot of contexts, that spinfer will consider individually 
- ioread32be(port->membase + e)
+ uart_in32(e, port)

@@
expression e1,e2,port;
@@

-       iowrite32be(e1, port->membase + e2);
+       uart_out32(e1, e2, port);
