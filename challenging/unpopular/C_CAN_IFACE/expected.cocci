@@
expression e,e1,e2,m1,m2;
@@

-	e = e1->read_reg(e1, C_CAN_IFACE(m1, e2));
-	e |= e1->read_reg(e1, C_CAN_IFACE(m2, e2)) << 16;
+	e = e1->read_reg32(e1, C_CAN_IFACE(m1, e2));

@@
expression e1,e2,e3,m1,m2;
@@

-	e1->write_reg(e1, C_CAN_IFACE(m1, e2), e3);
-	e1->write_reg(e1, C_CAN_IFACE(m2, e2), e3 >> 16);
+	e1->write_reg32(e1, C_CAN_IFACE(m1, e2), e3);
