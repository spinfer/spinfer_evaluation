@@
expression page;
@@

-				ClearPageReserved(page);
-				__free_page(page);
+				__free_reserved_page(page);

@@
expression page,e;
@@

				page = virt_to_page(e);
-				ClearPageReserved(page);
-				init_page_count(page);
-				free_page(e);
+				__free_reserved_page(page);
