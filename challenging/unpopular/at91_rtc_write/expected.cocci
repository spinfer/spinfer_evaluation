@@
expression e;
@@

-	at91_rtc_write(AT91_RTC_IER, e);
+	at91_rtc_write_ier(e);

@@
expression e;
@@

-	at91_rtc_write(AT91_RTC_IDR, e);
+	at91_rtc_write_idr(e);

@@
@@

- at91_rtc_read(AT91_RTC_IMR)
+ at91_rtc_read_imr()
