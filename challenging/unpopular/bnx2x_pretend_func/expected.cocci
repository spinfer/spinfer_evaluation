@@
expression bp;
@@

-	bnx2x_pretend_func(bp, 0);
 	bnx2x_clear_blocks_parity(bp);
 	bnx2x_enable_blocks_parity(bp);
-	bnx2x_pretend_func(bp, 1);
-	bnx2x_clear_blocks_parity(bp);
-	bnx2x_enable_blocks_parity(bp);
-	bnx2x_pretend_func(bp, BP_ABS_FUNC(bp));

@@
expression bp;
@@

-	bnx2x_pretend_func(bp, 0);
 	bnx2x_disable_blocks_parity(bp);
-	bnx2x_pretend_func(bp, 1);
-	bnx2x_disable_blocks_parity(bp);
-	bnx2x_pretend_func(bp, BP_ABS_FUNC(bp));
