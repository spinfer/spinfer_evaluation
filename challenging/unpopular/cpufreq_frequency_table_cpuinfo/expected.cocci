@@
expression policy,table,e1,e2;
identifier c;
@@

	unsigned int c = policy->cpu;
	... when != c = e1
-       cpufreq_frequency_table_cpuinfo
+       cpufreq_table_validate_and_show
		(policy, table)
	... when != c = e2
-       cpufreq_frequency_table_get_attr(table, c);

@@
expression policy,table;
@@

-       cpufreq_frequency_table_cpuinfo
+       cpufreq_table_validate_and_show
		(policy, table)
...
-       cpufreq_frequency_table_get_attr(table, policy->cpu);

@@
expression policy,table;
@@

-       cpufreq_frequency_table_get_attr(table, policy->cpu);
	...
-       cpufreq_frequency_table_cpuinfo(policy, table)
+       cpufreq_table_validate_and_show(policy, table)

// ----

@@ // not actually needed for examples
expression policy,table;
@@

-       cpufreq_frequency_table_cpuinfo
+       cpufreq_table_validate_and_show
		(policy, &table[0])
...
-       cpufreq_frequency_table_get_attr(table, policy->cpu);

@@
expression policy,table;
@@

-       cpufreq_frequency_table_get_attr(table, policy->cpu);
	...
-       cpufreq_frequency_table_cpuinfo
+       cpufreq_table_validate_and_show
		(policy, &table[0])
