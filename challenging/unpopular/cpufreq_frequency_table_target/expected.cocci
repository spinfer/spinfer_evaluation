@@
expression policy, old_freq;
@@

-	cpufreq_frequency_table_target(policy, old_freq,
-						    CPUFREQ_RELATION_H)
+	cpufreq_table_find_index_h(policy, old_freq)

@@
expression policy, old_freq;
@@

-	cpufreq_frequency_table_target(policy, old_freq,
-						    CPUFREQ_RELATION_L)
+	cpufreq_table_find_index_l(policy, old_freq)

@@
expression policy, old_freq;
@@

-	cpufreq_frequency_table_target(policy, old_freq,
-						    CPUFREQ_RELATION_N)
+	cpufreq_table_find_index_n(policy, old_freq)

@@
expression policy, old_freq;
@@

-	cpufreq_frequency_table_target(policy, old_freq,
-						    CPUFREQ_RELATION_C)
+	cpufreq_table_find_index_c(policy, old_freq)
