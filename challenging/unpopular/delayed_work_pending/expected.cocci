@@
expression e1,e2,e3;
@@
- if (!delayed_work_pending(e2))
  queue_delayed_work(e1,e2,e3);

@@
expression e1,e2;
@@
- if (!delayed_work_pending(e1))
  schedule_delayed_work(e1,e2);

@@
expression e;
@@
- if (delayed_work_pending(e))
  flush_work(e);

@@
expression e;
@@
- if (delayed_work_pending(e))
  cancel_delayed_work_sync(e);

@@
expression e;
@@
- if (delayed_work_pending(e))
  cancel_delayed_work(e);

//----------

@@
expression e1,e2,e3;
@@
- if (!delayed_work_pending(e2))
- {
   ... queue_delayed_work(e1,e2,e3); ...
- }

@@
expression e1,e2;
@@
- if (!delayed_work_pending(e1))
- {
   ... schedule_delayed_work(e1,e2); ... 
- }

@@
expression e;
@@
- if (delayed_work_pending(e))
- {
   ... flush_work(e); ...
- }

@@
expression e;
@@
- if (delayed_work_pending(e))
- {
   ... cancel_delayed_work_sync(e); ...
- }

@@
expression e;
@@
- if (delayed_work_pending(e))
- {
   ... cancel_delayed_work(e); ...
- }

// ----------------------------------------------------------------

@@
expression e1,e2;
@@
- if (!work_pending(e2))
  queue_work(e1,e2);

@@
expression e;
@@
- if (!work_pending(e))
  schedule_work(e);

@@
expression e;
@@
- if (work_pending(e))
  flush_work(e);

@@
expression e;
@@
- if (work_pending(e))
  cancel_work_sync(e);

@@
expression e;
@@
- if (work_pending(e))
  cancel_work(e);

//----------

@@
expression e1,e2;
@@
- if (!work_pending(e2))
- {
   ... queue_work(e1,e2); ...
- }

@@
expression e;
@@
- if (!work_pending(e))
- {
   ... schedule_work(e); ...
- }

@@
expression e;
@@
- if (work_pending(e))
- {
   ... flush_work(e); ...
- }

@@
expression e;
@@
- if (work_pending(e))
- {
   ... cancel_work_sync(e); ...
- }

@@
expression e;
@@
- if (work_pending(e))
- {
   ... cancel_work(e); ...
- }

// ----------------------------------------------------------------

@@
expression e,e1,e2,e3;
@@
- if (e && !delayed_work_pending(e2))
+ if (e)
  { ... queue_work(e1,e2,e3); ... }

@@
expression e,e1,e2;
@@
- if (e && !delayed_work_pending(e1))
+ if (e)
  { ... schedule_delayed_work(e1,e2); ... }

@@
expression e,e1;
@@
- if (e && delayed_work_pending(e1))
+ if (e)
  { ... flush_work(e1); ... }

@@
expression e,e1;
@@
- if (e && delayed_work_pending(e1))
+ if (e)
  { ... cancel_delayed_work_sync(e1); ... }

@@
expression e,e1;
@@
- if (e && delayed_work_pending(e1))
+ if (e)
  { ... cancel_delayed_work(e1); ... }

//----------


@@
expression e,e1,e2,e3;
@@
- if (!delayed_work_pending(e2) && e)
+ if (e)
  { ... queue_work(e1,e2,e3); ... }

@@
expression e,e1,e2;
@@
- if (!delayed_work_pending(e1) && e)
+ if (e)
  { ... schedule_delayed_work(e1,e2); ... }

@@
expression e,e1;
@@
- if (delayed_work_pending(e1) && e)
+ if (e)
  { ... flush_work(e1); ... }

@@
expression e,e1;
@@
- if (delayed_work_pending(e1) && e)
+ if (e)
  { ... cancel_delayed_work_sync(e1); ... }

@@
expression e,e1;
@@
- if (delayed_work_pending(e1) && e)
+ if (e)
  { ... cancel_delayed_work(e1); ... }

// ----------------------------------------------------------------

@@
expression e,e1,e2;
@@
- if (e && !work_pending(e2))
+ if (e)
  { ... queue_work(e1,e2); ... }

@@
expression e,e1;
@@
- if (e && !work_pending(e1))
+ if (e)
  { ... schedule_work(e1); ... }

@@
expression e,e1;
@@
- if (e && work_pending(e1))
+ if (e)
  { ... flush_work(e1); ... }

@@
expression e,e1;
@@
- if (e && work_pending(e1))
+ if (e)
  { ... cancel_work_sync(e1); ... }

@@
expression e,e1;
@@
- if (e && work_pending(e1))
+ if (e)
  { ... cancel_work(e1); ... }

//----------

@@
expression e,e1,e2;
@@
- if (!work_pending(e2) && e)
+ if (e)
  { ... queue_work(e1,e2); ... }

@@
expression e,e1;
@@
- if (!work_pending(e1) && e)
+ if (e)
  { ... schedule_work(e1); ... }

@@
expression e,e1;
@@
- if (work_pending(e1) && e)
+ if (e)
  { ... flush_work(e1); ... }

@@
expression e,e1;
@@
- if (work_pending(e1) && e)
+ if (e)
  { ... cancel_work_sync(e1); ... }

@@
expression e,e1;
@@
- if (work_pending(e1) && e)
+ if (e)
  { ... cancel_work(e1); ... }

// ----------------------------------------------------------------
