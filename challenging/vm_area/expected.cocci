@@
expression new,vma;
symbol vm_area_cachep;
@@

-	new = kmem_cache_alloc(vm_area_cachep, GFP_KERNEL);
+	new = vm_area_dup(vma);
	...
	*new = *vma;

@@
symbol vm_area_cachep;
@@

-	kmem_cache_zalloc(vm_area_cachep, GFP_KERNEL)
+	vm_area_alloc()

@@
expression vma;
symbol vm_area_cachep;
@@

-	kmem_cache_free(vm_area_cachep, vma);
+	vm_area_free(vma);
