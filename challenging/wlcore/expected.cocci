@r@
expression ret,wl;
@@

-	ret = wl1271_ps_elp_wakeup(wl);
+	ret = pm_runtime_get_sync(wl->dev);
	if (ret < 0)
	{
+		pm_runtime_put_noidle(wl->dev);
 		...
	}

@s@
expression ret,wl;
statement S;
@@

-	ret = wl1271_ps_elp_wakeup(wl);
-	if (ret < 0)
+	ret = pm_runtime_get_sync(wl->dev);
+	if (ret < 0) {
+		pm_runtime_put_noidle(wl->dev);
 		S
+	}

@@
expression wl;
@@

-	wl1271_ps_elp_sleep(wl);
+	pm_runtime_put(wl->dev);


@depends on r || s@
@@
- #include "ps.h"

@t depends on r || s@
@@

  #include <...>
+ #include <linux/pm_runtime.h>

@depends on (r || s) && !t@
@@

+ #include <linux/pm_runtime.h>
  #include "..."
