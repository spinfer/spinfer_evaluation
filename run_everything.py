#!/usr/bin/env python3
from pathlib import Path
import logging
from os import chdir
import re
from subprocess import run, CalledProcessError, STDOUT
from typing import List, Dict, Union, Optional, Tuple
import multiprocessing
from time import sleep, time
import traceback
from shutil import copy
import json
from sys import argv
import signal

RunResponse = Union[int, float, None]

PATCH_RE = re.compile(r'^@[^\n]*@[^@]*@@', re.MULTILINE | re.DOTALL)
SCORE_RE = re.compile(
    r'^\/\/ Final metrics \(for the combined \d+ rules\):\n'
    r'\/\/ -- Edit Location --\n'
    r'\/\/ Recall: (?P<rec_loc>\d\.\d+), Precision: (?P<pre_loc>\d\.\d+)\n'
    r'\/\/ -- Node Change --\n'
    r'\/\/ Recall: (?P<rec_node>\d\.\d+), Precision: (?P<pre_node>\d\.\d+)\n'
    r'\/\/ -- General --\n'
    r'\/\/ Functions fully changed: (?P<cov>\d+)\/(?P<tot>\d+)\((?P<per>\d+)%\)$',
    re.MULTILINE
)
DIFF_RE = re.compile(
    r'^ (?P<files>\d+) files? changed'
    r'(?:, (?P<add>\d+) insertions?\(\+\))?'
    r'(?:, (?P<del>\d+) deletions?\(-\))?$',
    re.MULTILINE)
SPINFER = Path('/home/lucas/spinfer/spinfer.native')
EVAL_PATCH = Path('/home/lucas/spinfer/eval_patch.native')

logging.basicConfig(format='%(levelname)s -- %(processName)s -- %(message)s', level=logging.INFO)


class TestError(Exception):
    def __init__(self, message: str) -> None:
        self.message = message


def add_to_dict_with_suffix(to_update, suffix: str, new_values) -> None:
    to_update.update({f'{key}.{suffix}': value for key, value in new_values.items()})


def index_get_files(index: Path) -> List[Tuple[str, str]]:
    files = []
    with open(index, 'rt') as f:
        for line in f:
            line = line.strip()
            if line.startswith('#'):
                continue
            elements = line.split()
            if len(elements) != 2:
                logging.critical(f'{index}: Invalid syntax')
                return []
            else:
                before, after = elements
                if ('.h' in before) or ('.h' in after):
                    logging.warning(f'{index}: Index contains header file')
                files.append((before, after))
    return files


class Task():
    def __init__(self, *, directory: Path, category: str, experiment: str):
        self.directory = directory
        self.category = category
        self.experiment = experiment
        self.success = False
        self.nickname = f'{directory.name}.{category}.{experiment}'
        if not self.directory.exists():
            logging.critical(f'{self.directory}: Does not exists')
            self.compute = self.cannot_compute
        if not self.directory.is_dir():
            logging.critical(f'{self.directory}: Not a directory')
            self.compute = self.cannot_compute

    def compute(self):
        raise NotImplementedError

    def compute_debug(self):
        try:
            return self.compute()
        except KeyboardInterrupt:
            raise
        except:
            traceback.print_exc()
            print()
            raise

    def collect(self):
        raise NotImplementedError

    def get_next_task(self):
        raise NotImplementedError

    def has_next_task(self) -> bool:
        raise NotImplementedError

    def cannot_compute(self):
        logging.critical(f'{self.directory}: No directory, will not compute task')
        return self

    def set_name(self):
        proc = multiprocessing.current_process()
        proc.name = self.nickname


class PatchTask(Task):
    def __init__(self, *args, patch_path: Path, index: Path, test_index: Path, **kwargs):
        super().__init__(*args, **kwargs)
        self.patch_path = patch_path
        self.patch = ""
        self.index = index
        self.test_index = test_index
        self.execution_time = 0

    def run_timer_for(self, f):
        start = time()
        res = f()
        end = time()
        self.execution_time = int(end - start)
        return res

    def extract_patch_count(self) -> Optional[int]:
        matches = PATCH_RE.findall(self.patch)
        return len(matches)

    def extract_patch_metrics(self) -> Dict[str, RunResponse]:
        match = SCORE_RE.search(self.patch)
        if match is not None:
            groups = match.groupdict(default='0')
            return {
                'recall_loc': float(groups['rec_loc']),
                'precision_loc': float(groups['pre_loc']),
                'recall_node': float(groups['rec_node']),
                'precision_node': float(groups['pre_node']),
                'covered_fun': int(groups['cov']),
                'total_fun': int(groups['tot']),
                'percentage': int(groups['per'])
            }
        else:
            logging.error(f'{self.directory / self.patch_path }: Cannot parse patch')
            return {key: None for key in [
                'recall_loc', 'precision_loc',
                'recall_node', 'precision_node',
                'covered_fun', 'total_fun', 'percentage'
            ]}

    def collect(self) -> Dict[str, RunResponse]:
        if self.success:
            patch_count = self.extract_patch_count()
            if not patch_count:  # None or 0
                logging.warning(f'{self.directory / self.patch_path}: Patch is empty, no metric to collect')
                metrics: Dict[str, RunResponse] = {
                    key: None for key in [
                        'recall_loc', 'precision_loc',
                        'recall_node', 'precision_node',
                        'covered_fun', 'total_fun', 'percentage'
                    ]
                }
            else:
                metrics = self.extract_patch_metrics()
            metrics.update({'patch_count': patch_count, 'execution_time': self.execution_time})
            return metrics
        else:
            return {key: None for key in [
                'patch_count', 'execution_time',
                'recall_loc', 'precision_loc',
                'recall_node', 'precision_node',
                'covered_fun', 'total_fun', 'percentage'
            ]}


class EvalHumainTask(PatchTask):
    def compute(self):
        self.set_name()
        chdir(self.directory)
        cmd: List[str] = [
            str(EVAL_PATCH),
            str(self.patch_path),
            str(self.index)
        ]
        try:
            result = self.run_timer_for(lambda: run(cmd, check=True, capture_output=True, text=True))
            with open(self.patch_path, 'rt') as f:
                self.patch = f.read() + '\n' + result.stdout
            self.success = True
        except CalledProcessError:
            logging.error(f'{self.directory / self.patch_path}: Eval patch crashed')
        return self

    def has_next_task(self):
        return False


class PureTask(PatchTask):
    def compute(self):
        self.set_name()
        chdir(self.directory)
        if not self.patch_path.exists():
            logging.critical(f'{self.directory / self.patch_path}: Does not exists')
            return self

        expected = Path('expected.cocci')
        if expected.exists():
            with open(expected, 'rt') as f:
                matches = PATCH_RE.findall(f.read())

            if len(matches) > 0:
                files = index_get_files(self.test_index)
                pure_index = f'{self.test_index}.{self.category}'
                file_group = f'file_group.{self.category}.{self.experiment}'

                with open(pure_index, 'wt') as findex, open(file_group, 'wt') as fgroup:
                    for before, after in files:
                        new_after = f'{before}.{self.category}.expected.c'
                        copy(before, new_after)
                        print(f'{before} {new_after}', file=findex)
                        print(f'{new_after}\n', file=fgroup)

                spatch_cmd: List[str] = [
                    'spatch',
                    '--sp-file',
                    str(expected),
                    '--file-groups',
                    file_group,
                    '--in-place',
                    '--very-quiet',
                    '--no-show-diff',
                ]
                self.run_timer_for(lambda: run(spatch_cmd, check=True))
            else:
                logging.warning(f'{self.directory / expected}: Invalid patch, running on raw')
                pure_index = self.test_index
        else:
            logging.warning(f'{self.directory / expected}: Does not exists, running on raw')
            pure_index = self.test_index


        cmd: List[str] = [
            str(EVAL_PATCH),
            str(self.patch_path),
            str(pure_index)
        ]
        try:
            result = run(cmd, check=True, capture_output=True, text=True)
            with open(self.patch_path, 'rt') as f:
                # Subsiting score hardcoded in patch
                self.patch = SCORE_RE.sub('', f.read()) + '\n' + result.stdout
            self.success = True
        except CalledProcessError:
            logging.error(f'{self.directory / self.patch_path}: Eval patch crashed')
        return self

    def has_next_task(self):
        return False


class SpinferTask(PatchTask):
    def __init__(self, *args, header_index: Optional[Path] = None, **kwargs):
        super().__init__(*args, **kwargs)
        self.header_index = header_index

    def compute(self):
        self.set_name()
        chdir(self.directory)
        if not self.index.exists():
            logging.critical(f'{self.directory / self.index}: Does not exists')
            return self
        if not self.test_index.exists():
            logging.critical(f'{self.directory / self.index}: Does not exists')
            return self
        cmd: List[str] = [
            str(SPINFER),
            '-f',
            str(self.index),
            '-o',
            str(self.patch_path),
            '--final-test',
            str(self.test_index),
            '--debug-patch-file',
            f'spinfer.debug.{self.experiment}',
        ]
        cmd = cmd + ['-h', str(self.header_index)] if self.header_index else cmd
        try:
            with open(f'spinfer.log.{self.experiment}', 'wt') as f:
                self.run_timer_for(lambda: run(cmd, check=True, stdout=f, stderr=STDOUT))
                if not self.patch_path.exists():
                    logging.critical(f'{self.directory / self.patch_path}: Does not exists')
                    self.success = False
                else:
                    with open(self.patch_path, 'rt') as f:
                        self.patch = f.read()
                        self.success = True
        except CalledProcessError:
            logging.error(f'{self.directory / self.patch_path}: Spinfer crashed')
        return self

    def has_next_task(self):
        return True

    def get_next_task(self):
        parameters = {
            'directory': self.directory,
            'category': 'pure',
            'experiment': self.experiment,
            'index': self.index,
            'test_index': self.test_index,
            'patch_path': self.patch_path,
        }
        if self.success:
            next_task = PureTask(**parameters)
            return next_task
        else:
            return None


def sigterm_handler(*args):
    raise KeyboardInterrupt


def main(directories_to_run):
    results: List[Dict[str, RunResponse]] = []
    threads_to_wait = 0
    data_backup = open('data_backup.json', 'wt')

    def get_tasks_for_directory(directory: Path):
        tasks = []
        index_full_normal = directory.joinpath('index_full')
        index_full_alternative = directory.joinpath('index')
        if index_full_normal.exists():
            full_files = index_get_files(index_full_normal)
            index_full = index_full_normal
        elif index_full_alternative.exists():
            logging.warning(f'{index_full_normal}: Does not exist, using {index_full_alternative} instead')
            full_files = index_get_files(index_full_alternative)
            index_full = index_full_alternative
        else:
            logging.critical(f'Neither {index_full_normal} or {index_full_alternative} are present')
            return []

        parameters = {
            'directory': directory,
            'category': 'raw',
            'experiment': 'full',
            'index': Path(index_full.name),
            'test_index': Path(index_full.name),
            'patch_path': Path('spinfer.cocci.full')
        }
        if directory.joinpath('hindex').exists():
            parameters.update({'header_index': 'hindex'})
        tasks.append(SpinferTask(**parameters))

        parameters = {
            'directory': directory,
            'category': 'humain',
            'experiment': 'generic',
            'index': Path(index_full.name),
            'test_index': Path(index_full.name),
            'patch_path': Path('expected.cocci')
        }
        if directory.joinpath('expected.cocci').exists():
            tasks.append(EvalHumainTask(**parameters))
        else:
            logging.critical(f'{directory.joinpath("expected.cocci")}: Does not exists')

        if len(full_files) > 2:
            reduced_name = 'index10' if len(full_files) > 20 else 'index_half'
            parameters = {
                'directory': directory,
                'category': 'raw',
                'experiment': 'reduced',
                'index': Path(reduced_name),
                'test_index': Path(f'test{reduced_name}'),
                'patch_path': Path('spinfer.cocci.reduced')
            }
            if directory.joinpath('hindex').exists():
                parameters.update({'header_index': 'hindex'})
            tasks.append(SpinferTask(**parameters))
        else:
            logging.info(f'{directory}: Less than 3 files, no reduced experiment')

        return tasks

    try:
        with multiprocessing.Pool(multiprocessing.cpu_count() // 2, maxtasksperchild=1) as pool:
            def add_to_pool(task):
                nonlocal threads_to_wait
                if task.has_next_task():
                    threads_to_wait += 1
                return pool.apply_async(
                    task.compute_debug,
                    callback=callback
                )

            # TODO: transform callback to iter on results
            def callback(task):
                nonlocal threads_to_wait
                task_info = {
                    'name': task.directory.name,
                    'category': task.category,
                    'experiment': task.experiment,
                }
                new_result = task.collect()
                new_result.update(task_info)
                results.append(new_result)
                print(json.dumps(new_result), file=data_backup, flush=True)
                if task.has_next_task():
                    next_task = task.get_next_task()
                    if next_task:
                        add_to_pool(next_task)
                    threads_to_wait -= 1

            waiting_list = []
            for d in directories_to_run:
                tasks = get_tasks_for_directory(Path(d))
                for task in tasks:
                    waiting_list.append(add_to_pool(task))

            for elt in waiting_list:
                elt.wait()
            sleep(10)

            pool.close()
            logging.info("No more jobs to add to pool")

    except KeyboardInterrupt:
        logging.info("Interrupt asked, terminating worker")

    pool.join()
    print(results)
    data_backup.close()


if __name__ == '__main__':
    _, directory_file = argv
    with open(directory_file, 'rt') as f:
        directories_to_run = f.read().splitlines()
#    signal.signal(signal.SIGTERM, sigterm_handler)
    main(directories_to_run)
